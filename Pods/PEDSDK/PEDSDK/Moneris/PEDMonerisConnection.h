//
//  PEDMonerisConnection.h
//  PEDSDK
//
//  Created by Phil Peters on 31/07/2019.
//  Copyright © 2019 Pay With Bolt. All rights reserved.
//

#import "PEDWhiteLabelledConnection.h"

NS_ASSUME_NONNULL_BEGIN

@interface PEDMonerisConnection : PEDWhiteLabelledConnection

- (id) initWithConfiguration:(NSDictionary *)configuration statusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate;

@end

NS_ASSUME_NONNULL_END
