//
//  PEDMonerisConnection
//  pedsdk
//
//  Created by Phil Peters on 31/07/2019.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import "PEDMonerisConnection.h"
#import "PEDPaymentRequest.h"
#import "PEDRefundRequest.h"
#import "PEDConnection+Private.h"
#import "PEDPaymentRequest+Private.h"
#import "PEDRefundRequest+Private.h"
#import "PEDPaymentViewController.h"
#import "PEDUtil.h"
#import "XMLReader.h"

@interface PEDMonerisConnection()

@property (nonatomic, assign) BOOL debug;

@property (nonatomic, assign) BOOL sandbox;
@property (nonatomic, retain) NSString * storeId;
@property (nonatomic, retain) NSString * apiToken;
@property (nonatomic, retain) NSString * postbackUrl;

@property (nonatomic, retain) NSURLSessionDataTask * pairTerminalDataTask;
@property (nonatomic, retain) NSURLSessionDataTask * initialiseTerminalDataTask;
@property (nonatomic, retain) NSURLSessionDataTask * paymentDataTask;
@property (nonatomic, retain) NSURLSessionDataTask * pollDataTask;
@property (nonatomic, assign) BOOL cancelPolling;

@property (nonatomic, retain) NSURLSessionDataTask * refundDataTask;
@property (nonatomic, retain) UIAlertController * cancelAlertView;
@end

@implementation PEDMonerisConnection


- (id) initWithConfiguration:(NSDictionary *)configuration statusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate {
    self = [super initWithConfiguration:configuration statusUpdateBlock:statusUpdateBlock];
    
    [self downloadConfig];
    
    self.debug = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"PEDSDKDEBUG"];
    
    return self;
}

- (void) downloadConfig {
    if (!self.status || [self.status isEqualToString:kPEDStatusConnectingFailed]) {
        self.status = kPEDStatusConnecting;
        PEDMonerisConnection * _self = self;
        
//        // Below is for testing
//        _self.sandbox = TRUE;
//        _self.storeId = @"gwca000558";
//        _self.apiToken = @"6HpvlEZwgeuhEU5DTYfn";
//        _self.postbackUrl = @"https://bpp-outerheaven.withbolt.com/b/BO001242/store";
//
//        if (self.selectedDevice) {
//            _self.status = kPEDStatusConnectedReady;
//        } else {
//            _self.status = kPEDStatusConnectedNoPed;
//        }
//
//        dispatch_async( dispatch_get_main_queue(), ^{
//            _self.statusUpdateBlock(_self);
//        });
//
//        return;
//        // End Testing
        
        NSURL * url = [NSURL URLWithString:[self.configuration valueForKeyPath:@"pedValues.configuration"]];
        
        if (self.debug) {
            NSLog(@"PEDMonerisConnection.downloadConfiguration: %@", url.absoluteString);
        }
        
        [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (error) {
                // Dont call callback because we can't handle an error without a PEDConnection
                // On network status change we should try to connect again
                _self.status = kPEDStatusConnectingFailed;
                _self.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Could not connect to server to retrieve credentials"]}];
            } else {
                NSDictionary * config = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                _self.configuration = config;

                _self.sandbox = [config[@"sandbox"] isEqual:@"TRUE"];
                _self.storeId = config[@"store_id"];
                _self.apiToken = config[@"api_token"];
                _self.postbackUrl = config[@"postback_url"];
                
                if (self.selectedDevice) {
                    _self.status = kPEDStatusConnectedReady;
                } else {
                    _self.status = kPEDStatusConnectedNoPed;
                }
                
                dispatch_async( dispatch_get_main_queue(), ^{
                    _self.statusUpdateBlock(_self);
                });
            }
        }] resume];
    }
}


// + txntype + terminal id
- (NSURLSessionDataTask *) monerisDataTaskType:(NSString *)txnType terminalId:(NSString *)terminalId withRequest:(NSDictionary *)request completionHander: (void (^)(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)) completionHander {
    
    NSLog(@"%@ %@ %@ %@", self.storeId, self.apiToken, txnType, terminalId);
    
    NSDictionary * body = @{
                            @"storeId": self.storeId,
                            @"apiToken": self.apiToken,
                            @"txnType": txnType,
                            @"postbackUrl": self.postbackUrl,
                            @"terminalId": terminalId,
                            @"request": request
                            };

    if (self.debug) {
        NSLog(@"Request [%@ %@]: %@", @"POST", self.endPoint, request ? [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:body options:0 error:nil] encoding:NSUTF8StringEncoding] : @"n/a");
    }

    NSMutableURLRequest * urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:self.endPoint]];
    urlRequest.HTTPMethod = @"POST";
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    urlRequest.HTTPBody =  [NSJSONSerialization dataWithJSONObject:body options:0 error:nil];
    
    NSURLSessionDataTask * dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            NSLog(@"PEDMonerisConnection Result [%@] ERROR: %@", self.endPoint, error.localizedDescription);
        } else if (self.debug || TRUE) {
            NSLog(@"Result [%@]: %@", self.endPoint, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        }
        
        completionHander(data, response, error);
    }];
    
    return dataTask;
}


-(UIAlertController *) showSpinner:(NSString *)title onCancel:(void (^)(void)) cancelBlock {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                     message:@"\n\n"
                                                              preferredStyle:UIAlertControllerStyleAlert];
    
    UIActivityIndicatorView* indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    indicator.color = [UIColor blackColor];
    indicator.translatesAutoresizingMaskIntoConstraints=NO;
    [alert.view addSubview:indicator];
    
    
    NSLayoutConstraint * constraintsHorizontal = [NSLayoutConstraint constraintWithItem:indicator
                                                                 attribute:NSLayoutAttributeCenterX
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:indicator.superview
                                                                 attribute:NSLayoutAttributeCenterX
                                                                multiplier:1.f constant:0.f];
    NSLayoutConstraint * constraintsVertical = [NSLayoutConstraint constraintWithItem:indicator
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:indicator.superview
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.f constant:0.f];
    NSArray * constraints = @[constraintsHorizontal, constraintsVertical];
    [alert.view addConstraints:constraints];
    [indicator setUserInteractionEnabled:NO];
    [indicator startAnimating];
    
    UIAlertAction * ok = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action) {
                                                    cancelBlock();
                                                }];
    [alert addAction:ok];

    dispatch_async( dispatch_get_main_queue(), ^{
        [self.paymentViewController presentViewController:alert animated:NO completion:nil];
    });
    
    return alert;
}


-(void) pairDeviceWithTerminalId:(NSString *)terminalId andPairingToken:(NSString *)pairingToken onCompletion:(void (^)(PEDWhiteLabelledConnection *, NSError *)) completionBlock {
    PEDMonerisConnection *_self = self;
    
    NSLog(@"pairDeviceWithTerminalId:%@ andPairingToken:%@", terminalId, pairingToken);
    
    UIAlertController * spinner = [self showSpinner:@"Pairing" onCancel:^{
        _self.cancelPolling = true;
        [self.pairTerminalDataTask cancel];
        // Cancel polling
    }];
    
    self.pairTerminalDataTask = [self monerisDataTaskType:@"pair" terminalId:terminalId withRequest:@{@"terminalId": terminalId, @"pairingToken": pairingToken} completionHander:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        self.pairTerminalDataTask = nil;

        if (error) {
            dispatch_async( dispatch_get_main_queue(), ^{
                [spinner dismissViewControllerAnimated:NO completion:nil];
                completionBlock(_self, error);
            });
        } else {
            NSDictionary * response = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            
            if ([[response valueForKeyPath:@"receipt.Error"] isEqualToString:@"true"]) {
                if ([[response valueForKeyPath:@"receipt.ResponseCode"] isEqualToString:@"902"]) {
                    // Invalid Terminal ID ()
                    error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk.moneris.invalidTerminalId" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Invalid Terminal ID"]}];
                } else if ([[response valueForKeyPath:@"receipt.ResponseCode"] isEqualToString:@"903"]) {
                   error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk.moneris.deviceNotPaired" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Invalid Pairing Token / Could Not Connect"]}];
                } else if ([[response valueForKeyPath:@"receipt.ResponseCode"] isEqualToString:@"904"]) {
                    error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk.moneris.deviceBusy" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Device busy"]}];
                } else {
                    error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Pair %@: %@", [response valueForKeyPath:@"receipt.ResponseCode"], [response valueForKeyPath:@"receipt.Message"]]}];
                }
                
                dispatch_async( dispatch_get_main_queue(), ^{
                    [spinner dismissViewControllerAnimated:NO completion:nil];
                    completionBlock(_self, error);
                });
            } else {
                [_self pollForTicket:[response valueForKeyPath:@"receipt.CloudTicket"] until:[NSDate dateWithTimeIntervalSinceNow:60] onCompletion:^(NSError *error, NSDictionary * response) {
                    if (error) {
                        dispatch_async( dispatch_get_main_queue(), ^{
                            [spinner dismissViewControllerAnimated:NO completion:nil];
                            completionBlock(_self, error);
                        });
                    } else {
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            [spinner dismissViewControllerAnimated:TRUE completion:nil];
                            [_self initialiseDeviceWithTerminalId:terminalId onCompletion:completionBlock];
                        });
                    }
                }];
            }
        }
    }];
    
    [self.pairTerminalDataTask resume];
}

-(void) initialiseDeviceWithTerminalId:(NSString *)terminalId onCompletion:(void (^)(PEDWhiteLabelledConnection *, NSError *)) completionBlock {
    PEDMonerisConnection *_self = self;

    UIAlertController * spinner = [self showSpinner:@"Initializing" onCancel:^{
        _self.cancelPolling = true;
        
        [self.initialiseTerminalDataTask cancel];
        // Cancel polling
        
    }];

    NSLog(@"initialiseDeviceWithTerminalId: %@", terminalId);
    self.initialiseTerminalDataTask = [self monerisDataTaskType:@"initialization" terminalId:terminalId withRequest:@{@"terminalId": terminalId} completionHander:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {

        if (error) {
            // Return error
        } else {
            NSDictionary * response = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];

            if (![[response valueForKeyPath:@"receipt.Error"] isEqualToString:@"false"]) {
                if ([[response valueForKeyPath:@"receipt.ResponseCode"] isEqualToString:@"903"]) {
                    // Unable to locate pinpad - its not paired
                    error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk.moneris.deviceNotPaired" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Device not paired / could not connect"]}];
                } else if ([[response valueForKeyPath:@"receipt.ResponseCode"] isEqualToString:@"904"]) {
                    error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk.moneris.deviceBusy" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Device busy"]}];
                } else {
                    error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk.moneris.unknown" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Initialize failed: %@", error.localizedDescription]}];
                }
                    
            } else {
                [_self pollForTicket:[response valueForKeyPath:@"receipt.CloudTicket"] until:[NSDate dateWithTimeIntervalSinceNow:120] onCompletion:^(NSError *error, NSDictionary * response) {
                    if (error) {
                        error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk.moneris.timeout" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Initialize Timeout"]}];
                    }
                    dispatch_async( dispatch_get_main_queue(), ^{
                        [spinner dismissViewControllerAnimated:NO completion:nil];
                       completionBlock(_self, error);
                    });
                }];
            }
        }
        
        if (error) {
            dispatch_async( dispatch_get_main_queue(), ^{
                [spinner dismissViewControllerAnimated:NO completion:nil];
                completionBlock(_self, error);
            });
        }
    }];

    [self.initialiseTerminalDataTask resume];
}



- (void) reachabilityChanged:(NSObject *) notification {
    if ([self.status isEqualToString:kPEDStatusConnectingFailed]) {
        [self downloadConfig];
    }
    
    [super reachabilityChanged:notification];
}

- (NSString *) pedType {
    return kPEDTypeMoneris;
}

- (NSString *) pedName {
    return @"Moneris";
}
- (NSString *) pedDescription {
    return self.selectedDevice ? [NSString stringWithFormat:@"%@", self.selectedDevice[@"name"]] : nil;
}

- (NSDictionary *) currencies {
    return @{}; // USD only
}

- (BOOL) canRefundFull {
    return true;    
}
- (BOOL) canRefundPartial {
    return true;
}

- (NSString *) endPoint {
    return self.sandbox ? @"https://ippostest.moneris.com/Terminal/" : @"https://ippos.moneris.com/Terminal/";
}


- (PEDPaymentRequest *) startPaymentInCurrency:(NSString*)currency forAmount:(NSDecimalNumber *)amount withReference:(NSString *)reference options:(NSDictionary *)options statusUpdateBlock:(void (^)(PEDPaymentRequest *))statusUpdateBlock {
    
    if (![currency isEqualToString:@"CAD"]) {
        PEDPaymentRequest * paymentRequest = [[PEDPaymentRequest alloc] initWithConnection:self forAmount:amount inCurrency:currency withReference:reference statusUpdateBlock:statusUpdateBlock];

        paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk.moneris.currency" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Moneris only supports CAD, and this payment is in %@", paymentRequest.currency]}];
        paymentRequest.status = kPEDPaymentStatusInvalidAmount;
        return paymentRequest;
    } else {
        return [super startPaymentInCurrency:currency forAmount:amount withReference:reference options:options statusUpdateBlock:statusUpdateBlock];
    }
}

- (void) startPaymentRequest:(PEDPaymentRequest*)paymentRequest {
    paymentRequest.cancelRequested = false;
    paymentRequest.error = nil;
    paymentRequest.status = kPEDPaymentStatusInProgress;

    PEDMonerisConnection *_self = self;
    
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setMinimumFractionDigits:2];
    [formatter setMaximumFractionDigits:2];
    [formatter setGroupingSeparator:@""];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [formatter setLocale:usLocale];
    
    NSString * formattedAmount = [formatter stringFromNumber:paymentRequest.amount];
    
    if (self.cancelAlertView) {
        dispatch_async( dispatch_get_main_queue(), ^{
            [self.cancelAlertView dismissViewControllerAnimated:true completion:^{
                paymentRequest.status = kPEDPaymentStatusCancelled;
                _self.cancelAlertView = nil;
            }];
        });
    }

    NSLog(@"startPaymentRequest: %@", paymentRequest.amount);
    self.paymentDataTask = [self monerisDataTaskType:@"purchase" terminalId:self.selectedDevice[@"id"] withRequest:@{@"orderId": paymentRequest.reference, @"amount": formattedAmount} completionHander:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (error) {
            paymentRequest.error = error;
            paymentRequest.status = kPEDStatusConnectingFailed;
        } else {
            NSDictionary * response = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];

            if (![[response valueForKeyPath:@"receipt.Error"] isEqualToString:@"false"]) {
                if ([[response valueForKeyPath:@"receipt.ResponseCode"] isEqual:@"902"]) {
                    // Unable to locate pinpad - its not paired
                    paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk.moneris.deviceNotPaired" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Device not paired"]}];
                    paymentRequest.status = kPEDPaymentStatusError;
                } else {
                    paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@: %@", [response valueForKeyPath:@"receipt.ResponseCode"], [response valueForKeyPath:@"receipt.Message"]]}];
                    paymentRequest.status = kPEDPaymentStatusError;
                }
            } else {
                [_self pollForTicket:[response valueForKeyPath:@"receipt.CloudTicket"] until:[NSDate dateWithTimeIntervalSinceNow:120] onCompletion:^(NSError *error, NSDictionary * response) {
                    paymentRequest.paymentValues = @{
                        @"raw": response ? response : [NSNull null]
                    };

                    if (_self.cancelAlertView) {
                        dispatch_async( dispatch_get_main_queue(), ^{
                            [_self.cancelAlertView dismissViewControllerAnimated:FALSE completion:^{
                                _self.cancelAlertView = nil;
                            }];
                        });
                    }
                    
                    if (error) {
                        paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:@"Implementation error, unhandled response."}];
                        paymentRequest.status = kPEDPaymentStatusError;
                    } else {
                        if (![[response valueForKeyPath:@"receipt.Error"] isEqual:@"false"] || [[response valueForKeyPath:@"receipt.ResponseCode"] intValue] >= 50) {

                            if ([[response valueForKeyPath:@"receipt.ErrorCode"] isEqual:@"401"]) {
                                // Unable to locate pinpad - its not paired
                                paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk.moneris.deviceNotPaired" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Cancelled by User"]}];
                                paymentRequest.status = kPEDPaymentStatusCancelled;
                            } else if (![[response valueForKeyPath:@"receipt.Error"] isEqual:@"false"]) {
                                paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk.moneris.declined" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Payment Declined: %@", [response valueForKeyPath:@"receipt.Error"]]}];
                                paymentRequest.status = kPEDPaymentStatusDeclined;
                                
                            } else {
                                paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk.moneris.declined" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Payment Declined: %@", [response valueForKeyPath:@"receipt.ResponseCode"]]}];
                                paymentRequest.status = kPEDPaymentStatusDeclined;
                            }
                        } else {
                            NSLog(@"Payment Success");
                            
                            paymentRequest.methodType = [response valueForKeyPath:@"receipt.CardName"];
                            paymentRequest.methodDescription = [response valueForKeyPath:@"receipt.Pan"] ;
                            paymentRequest.authorisationCode = [response valueForKeyPath:@"receipt.AuthCode"];
                            paymentRequest.gatewayReference = [response valueForKeyPath:@"receipt.ReferenceNumber"];
                            paymentRequest.paymentValues = @{
                                                             @"transactionId": paymentRequest.gatewayReference ? paymentRequest.gatewayReference : [NSNull null],
                                                             @"paymentMethodType": paymentRequest.methodType ? paymentRequest.methodType : [NSNull null],
                                                             @"paymentMethodDetail": paymentRequest.methodDescription ? paymentRequest.methodDescription : [NSNull null],
                                                             
                                                             @"authCode": paymentRequest.authorisationCode ? paymentRequest.authorisationCode : [NSNull null],
                                                             @"receiptId": [response valueForKeyPath:@"receipt.ReceiptId"],
                                                             @"transID": [response valueForKeyPath:@"receipt.TransId"],
                                                             @"raw": response ? response : [NSNull null]
                                                             };
                            paymentRequest.error = nil;
                            paymentRequest.status = kPEDPaymentStatusSuccess;
                        }
                    }
                }];
                
            }
        }
        
        dispatch_async( dispatch_get_main_queue(), ^{
            _self.statusUpdateBlock(_self);
        });
    }];
    
    [self.paymentDataTask resume];
}



-(void) pollForTicket:(NSString *)cloudTicket until:(NSDate *)until onCompletion:(void (^)(NSError * error, NSDictionary *)) completionBlock{
    PEDMonerisConnection * _self = self;

    if (self.cancelPolling) {
        self.cancelPolling = false;
        completionBlock([NSError errorWithDomain:@"com.paywithbolt.pedsdk.cancelled" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"User Cancelled"]}], nil);
        return;
    }
    
    NSString * pollUrl = [NSString stringWithFormat:@"%@/%@?%f", self.postbackUrl, cloudTicket, [NSDate date].timeIntervalSince1970];
    NSLog(@"pollForTicket [%@]: %@", cloudTicket, pollUrl);

    self.pollDataTask = [[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:pollUrl] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {

        if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
            NSInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
            
            if (statusCode != 200) {
                error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"HTTP Status Code %ld", statusCode]}];
            }
        }
        
        if (error) {
            NSLog(@"Error: %@", error.localizedDescription);
            // retry
            if ([until compare:[NSDate date]] > 0) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [_self pollForTicket:cloudTicket until:until onCompletion:completionBlock];
                });
            } else {
                completionBlock([NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Timeout waiting for cloud ticket: %@", cloudTicket]}], nil);
            }
        } else {
            NSLog(@"pollForTicket [%@]: %@", cloudTicket, [NSString stringWithUTF8String:[data bytes]]);
            NSDictionary * response = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            
            completionBlock(nil, response);
        }
    }];

    [self.pollDataTask resume];
}


- (PEDRefundRequest *) refundPaymentReference:(NSString*)reference amount:(NSDecimalNumber *)amount currency:(NSString *)currency withReason:(NSString *)reason statusUpdateBlock:(void(^)(PEDRefundRequest * request))statusUpdateBlock {
    
    PEDRefundRequest * refundRequest = [[PEDRefundRequest alloc] initWithConnection:self forTransactionReference:reference forAmount:amount inCurrency:currency withReason:reason statusUpdateBlock:statusUpdateBlock];
    
    if (!reference.length) {
        NSLog(@"Refund failed!: Reference Required.");
        refundRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Reference required."]}];
        refundRequest.status = kPEDPaymentStatusError;
    } else if (amount && ([amount isEqual:[NSDecimalNumber notANumber]] || (!amount.doubleValue))) {
        NSLog(@"Refund failed!: Invalid amount.");
        refundRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Invalid amount."]}];
        refundRequest.status = kPEDPaymentStatusError;
    } else {
        NSError *error;
        NSDictionary * referenceDict = [NSJSONSerialization JSONObjectWithData:[reference dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];

        if (error) {
            NSLog(@"Could not decode reference: %@", [reference dataUsingEncoding:NSUTF8StringEncoding]);
            refundRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Could not decode reference"]}];
            refundRequest.status = kPEDPaymentStatusError;
        } else {
            PEDMonerisConnection *_self = self;
            
            NSNumberFormatter *formatter = [NSNumberFormatter new];
            [formatter setMinimumFractionDigits:2];
            [formatter setMaximumFractionDigits:2];
            [formatter setGroupingSeparator:@""];
            NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
            [formatter setLocale:usLocale];
            
            NSString * formattedAmount = [formatter stringFromNumber:amount];
            
            NSString * body = [NSString stringWithFormat: @"<request><store_id>%@</store_id><api_token>%@</api_token><refund><order_id>%@</order_id><amount>%@</amount><txn_number>%@</txn_number><crypt_type>7</crypt_type></refund></request>", self.storeId, self.apiToken, [referenceDict valueForKeyPath:@"order_id"], formattedAmount, [referenceDict valueForKeyPath:@"txn_number"]];
            
            NSString * url = self.sandbox ? @"https://esqa.moneris.com/gateway2/servlet/MpgRequest" : @"https://www3.moneris.com/gateway2/servlet/MpgRequest";
            
            if (self.debug) {
                NSLog(@"Request [%@ %@]: %@", @"POST", url, body);
            }
            
            NSMutableURLRequest * urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
            urlRequest.HTTPMethod = @"POST";
            [urlRequest addValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
            urlRequest.HTTPBody =  [body dataUsingEncoding:NSUTF8StringEncoding];
            
            self.refundDataTask = [[NSURLSession sharedSession] dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {

                if (error) {
                    NSLog(@"PEDMonerisConnection Result [%@] ERROR: %@", _self.endPoint, error.localizedDescription);
                    refundRequest.status = kPEDPaymentStatusError;
                } else if (_self.debug || TRUE) {
//                    NSLog(@"Result [%@]: %@", self.endPoint, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                    
                    NSDictionary *dict = [XMLReader dictionaryForXMLData:data
                                                                 options:XMLReaderOptionsProcessNamespaces
                                                                   error:&error];
                    
                    NSLog(@"JSON: %@", [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dict options:0 error:nil] encoding:NSUTF8StringEncoding]);
                    
                    NSInteger responseCode = [[dict valueForKeyPath:@"response.receipt.ResponseCode.text"] isEqualToString:@"null"] ? -1 : [NSDecimalNumber decimalNumberWithString:[dict valueForKeyPath:@"response.receipt.ResponseCode.text"]].integerValue;
                    
                    NSLog(@"Response Code: %ld", responseCode);
                    
                    if (!(responseCode >= 0 && responseCode <= 49)) {
                        refundRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[dict valueForKeyPath:@"response.receipt.Message.text"]}];
                        refundRequest.status = kPEDPaymentStatusDeclined;
                    } else {
                        refundRequest.gatewayReference = [dict valueForKeyPath:@"response.receipt.TransID.text"];
                        refundRequest.authorisationCode = [dict valueForKeyPath:@"response.receipt.AuthCode.text"];
                        refundRequest.error = nil;
                        refundRequest.status = kPEDPaymentStatusSuccess;
                    }
                }
            }];
            
            [self.refundDataTask resume];
        }
    }
    
    return refundRequest;
}

- (void) cancelPayment:(PEDPaymentRequest *)paymentRequest {
    if ([paymentRequest.status isEqualToString:kPEDPaymentStatusInProgress]) {
        self.cancelAlertView = [self showSpinner:@"Press Cancel on Device " onCancel:^{
        }];
    } else {
        if (self.cancelAlertView) {
            PEDMonerisConnection * _self = self;
            dispatch_async( dispatch_get_main_queue(), ^{
                [_self.cancelAlertView dismissViewControllerAnimated:FALSE completion:^{
                    paymentRequest.status = kPEDPaymentStatusCancelled;
                    _self.cancelAlertView = nil;
                }];
            });
        } else {
            paymentRequest.status = kPEDPaymentStatusCancelled;
        }
    }
}


- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    return false;
}

- (NSArray *) deviceList {
    NSMutableArray *devices = [NSMutableArray array];

    NSString * deviceId = [[NSUserDefaults standardUserDefaults] stringForKey:@"PEDSDKDeviceId"];

    if (deviceId) {
        [devices addObject:@{
                             @"id": deviceId,
                             @"name": [NSString stringWithFormat:@"Device %@", deviceId]
                             }];
    }
    
    [devices addObject:@{
                         @"id": @"NEW",
                         @"name": @"Pair New Device"
                         }];

    [devices addObject:@{
                         @"id": @"EXISTING",
                         @"name": @"Add Paired Device"
                         }];

    return devices;
}

- (void) setSelectedDevice:(NSDictionary *)device duringActivePaymentRequest:(PEDPaymentRequest *) paymentRequest{
    NSLog(@"setSelectedDevice:%@ duringActivePaymentRequest:", device[@"id"]);
    
    if ([device[@"id"] isEqualToString:@"NEW"]) {
        [self addDeviceDuringActivePaymentRequest:paymentRequest terminalId:self.selectedDevice[@"id"   ] pairingToken:@""];
    } else if ([device[@"id"] isEqualToString:@"EXISTING"]) {
        [self addExistingDeviceDuringActivePaymentRequest:paymentRequest terminalId:@""];
    } else {
        [super setSelectedDevice:device duringActivePaymentRequest:paymentRequest];
    }
}

-(void) addDeviceDuringActivePaymentRequest:(PEDPaymentRequest *) paymentRequest terminalId:(NSString *)terminalId pairingToken:(NSString *)pairingToken{
    PEDMonerisConnection * _self = self;
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Pair New Device"
                                  message:@"Enter the terminal ID and on screen pairing code"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
           handler:^(UIAlertAction * action) {
               NSString * terminalId = [[alert textFields] firstObject].text;
               NSString * pairingToken = [[alert textFields] lastObject].text;

               NSLog(@"addDeviceDuringActivePaymentRequest TerminalId: %@, Pairing Code: %@", terminalId, pairingToken);
               [self pairDeviceWithTerminalId:terminalId andPairingToken:pairingToken onCompletion:^(PEDWhiteLabelledConnection * connection, NSError * error) {
                   if (!error) {
                       [self setSelectedDevice:@{@"id": terminalId, @"name": [NSString stringWithFormat:@"Device %@", self.selectedDevice[@"id"]]}];
                       
                       _self.status = kPEDStatusConnectedReady;
                       _self.error = nil;
                       
                       dispatch_async( dispatch_get_main_queue(), ^{
                           _self.statusUpdateBlock(_self);
                           [_self startPaymentRequest:paymentRequest];
                       });
                   } else {
                       UIAlertController * alert = [UIAlertController
                                                    alertControllerWithTitle:@"Sorry!"
                                                    message:[NSString stringWithFormat:@"Pairing device failed (Error: %@)", error.localizedDescription]
                                                    preferredStyle:UIAlertControllerStyleAlert];
                       
                       UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action) {
                                                                       [_self addDeviceDuringActivePaymentRequest:paymentRequest terminalId:terminalId pairingToken:pairingToken];
                                                                   }];
                       [alert addAction:ok];

                       dispatch_async( dispatch_get_main_queue(), ^{
                           [self.paymentViewController presentViewController:alert animated:YES completion:nil];
                       });
                   }
               }];
           }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.text = terminalId;
        textField.placeholder = @"Terminal Id";
    }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.text = pairingToken;
        textField.placeholder = @"Pairing Code";
    }];
    
    [self.paymentViewController presentViewController:alert animated:YES completion:nil];
}

-(void) addExistingDeviceDuringActivePaymentRequest:(PEDPaymentRequest *) paymentRequest terminalId:(NSString *)terminalId {
    PEDMonerisConnection * _self = self;

    UIAlertController * alert= [UIAlertController
                                  alertControllerWithTitle:@"Add Paired Device"
                                  message:@"Enter the terminal ID"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
           handler:^(UIAlertAction * action) {
               NSString * terminalId = [[alert textFields] firstObject].text;
               
               NSLog(@"addExistingDeviceDuringActivePaymentRequest TerminalId: %@", terminalId);
               [self initialiseDeviceWithTerminalId:terminalId onCompletion:^(PEDWhiteLabelledConnection * connection, NSError *error) {
                   if (!error) {
                       [self setSelectedDevice:@{@"id": terminalId, @"name": [NSString stringWithFormat:@"Device %@", self.selectedDevice[@"id"]]}];
                       
                       _self.status = kPEDStatusConnectedReady;
                       _self.error = nil;
                       
                       dispatch_async( dispatch_get_main_queue(), ^{
                           _self.statusUpdateBlock(_self);
                           [_self startPaymentRequest:paymentRequest];
                       });
                   } else {
                       UIAlertController * alert = [UIAlertController
                                                   alertControllerWithTitle:@"Sorry!"
                                                    message:[NSString stringWithFormat:@"Initializing device failed (Error: %@)", error.localizedDescription]
                                                   preferredStyle:UIAlertControllerStyleAlert];

                       UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                                                                      [_self addExistingDeviceDuringActivePaymentRequest:paymentRequest terminalId:terminalId];
                                                                  }];
                       [alert addAction:ok];

                       dispatch_async( dispatch_get_main_queue(), ^{
                           [self.paymentViewController presentViewController:alert animated:YES completion:nil];
                       });
                   }
               }];
           }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.text = terminalId;
        textField.placeholder = @"Terminal Id";
    }];
    
    [self.paymentViewController presentViewController:alert animated:YES completion:nil];
}

- (BOOL) hideRefreshDeviceList {
    return true;
}


@end

