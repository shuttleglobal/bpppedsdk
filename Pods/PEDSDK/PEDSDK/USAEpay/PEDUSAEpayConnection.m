//
//  PEDUSAEpayConnection.m
//  pedsdk
//
//  Created by Phil Peters on 31/07/2019.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import "PEDUSAEpayConnection.h"
#import "PEDPaymentRequest.h"
#import "PEDRefundRequest.h"
#import "PEDConnection+Private.h"
#import "PEDPaymentRequest+Private.h"
#import "PEDRefundRequest+Private.h"
#import "PEDUtil.h"
#import <CommonCrypto/CommonDigest.h>

@interface PEDUSAEpayConnection()

@property (nonatomic, assign) BOOL debug;

@property (nonatomic, assign) BOOL sandbox;
@property (nonatomic, retain) NSString * pin;
@property (nonatomic, retain) NSString * apiKey;
@property (nonatomic, retain) NSArray * connectedTerminals;

@property (nonatomic, retain) NSURLSessionDataTask * connectedTerminalsDataTask;
@property (nonatomic, retain) NSURLSessionDataTask * paymentDataTask;
@property (nonatomic, retain) NSURLSessionDataTask * pollDataTask;
@property (nonatomic, retain) NSURLSessionDataTask * refundDataTask;
@property (nonatomic, retain) NSURLSessionDataTask * cancelDataTask;

@end

@implementation PEDUSAEpayConnection


- (id) initWithConfiguration:(NSDictionary *)configuration statusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate {
    self = [super initWithConfiguration:configuration statusUpdateBlock:statusUpdateBlock];
    
    [self downloadConfig];
    
    self.debug = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"PEDSDKDEBUG"];
    
    return self;
}

- (void) downloadConfig {
    if (!self.status || [self.status isEqualToString:kPEDStatusConnectingFailed]) {
        self.status = kPEDStatusConnecting;
        PEDUSAEpayConnection * _self = self;
        
//        dispatch_async( dispatch_get_main_queue(), ^{
//            _self.sandbox = TRUE;
//            _self.pin = @"123456";
//            _self.apiKey = @"_BE8NrcgrXm6TtY81b5Ary18JC8BuvQf";
//            _self.status = kPEDStatusConnectedReady;
//            _self.statusUpdateBlock(_self);
//            [_self refreshDeviceListOnCompletion:^(PEDWhiteLabelledConnection * _Nonnull connection) {
//                // Select first device?
//            }];
//        });
//
//        return;
        
        NSURL * url = [NSURL URLWithString:[self.configuration valueForKeyPath:@"pedValues.configuration"]];
        
        if (self.debug) {
            NSLog(@"PEDUSAEpayConnection.downloadConfiguration: %@", url.absoluteString);
        }
        
        [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (error) {
                // Dont call callback because we can't handle an error without a PEDConnection
                // On network status change we should try to connect again
                _self.status = kPEDStatusConnectingFailed;
                _self.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Could not connect to server to retrieve credentials"]}];
            } else {
                NSDictionary * config = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                _self.configuration = config;
                
                dispatch_async( dispatch_get_main_queue(), ^{
                    _self.sandbox = [config[@"sandbox"] isEqual:@"TRUE"];
                    _self.apiKey = config[@"sourceKey"];
                    _self.pin = config[@"sourcePin"];
                    _self.status = kPEDStatusConnectedReady;
                    _self.statusUpdateBlock(_self);
                    [_self refreshDeviceListOnCompletion:^(PEDWhiteLabelledConnection * _Nonnull connection) {
                        // Select first device?
                    }];
                });
                
            }
        }] resume];
    }
}


-(NSString*)sha256:(NSString*)input
{
    const char* str = [input UTF8String];
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(str, (unsigned int)strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_SHA256_DIGEST_LENGTH; i++)
    {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}

- (NSString*)base64:(NSString*)fromString {
    NSData *plainData = [fromString dataUsingEncoding:NSUTF8StringEncoding];
    return [plainData base64EncodedStringWithOptions:kNilOptions];  // iOS 7+
}

- (NSURLSessionDataTask *) usaepayDataTask:(NSString *)url method:(NSString *)method withRequest:(NSDictionary *)request completionHander: (void (^)(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)) completionHander {
    
    if (self.debug) {
        NSLog(@"Request [%@ %@]: %@", method, url, request ? [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:request options:0 error:nil] encoding:NSUTF8StringEncoding] : @"n/a");
    }
    
    NSString * seed = [[NSUUID UUID] UUIDString];
    NSString * prehash = [NSString stringWithFormat:@"%@%@%@", self.apiKey, seed, self.pin];
    NSString * apihash = [NSString stringWithFormat:@"s2/%@/%@", seed, [self sha256:prehash]    ];
    
    NSLog(@"Auth: %@ %@ %@ (%@)", seed, prehash, [NSString stringWithFormat:@"%@:%@", self.apiKey, apihash], [self  base64:[NSString stringWithFormat:@"%@:%@", self.apiKey, apihash]]);
    NSMutableURLRequest * urlRequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:url]];
    urlRequest.HTTPMethod = method;
    [urlRequest addValue:[NSString stringWithFormat:@"Basic %@", [self  base64:[NSString stringWithFormat:@"%@:%@", self.apiKey, apihash]]] forHTTPHeaderField:@"Authorization"];
    if ([method isEqualToString:@"POST"]){
        [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        urlRequest.HTTPBody =  [NSJSONSerialization dataWithJSONObject:request options:0 error:nil];
    }
    
    NSURLSessionDataTask * dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            NSLog(@"PEDUSAEpayConnection Result [%@] ERROR: %@", url, error.localizedDescription);
        } else if (self.debug) {
            NSLog(@"Result [%@]: %@", url, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        }
        
        completionHander(data, response, error);
    }];
    
    return dataTask;
}



-(void) refreshDeviceListOnCompletion:(void (^)(PEDWhiteLabelledConnection *)) completionBlock {
    PEDUSAEpayConnection *_self = self;
    self.connectedTerminalsDataTask = [self usaepayDataTask:[NSString stringWithFormat:@"%@/paymentengine/devices", self.endPoint] method:@"GET" withRequest:nil completionHander:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (error) {
            _self.status = kPEDStatusConnectingFailed;
            _self.error = error;
        } else {
            NSDictionary * result = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            _self.connectedTerminals = result[@"data"];
            _self.connectedTerminalsDataTask = nil;
            
            dispatch_async( dispatch_get_main_queue(), ^{
                _self.statusUpdateBlock(_self);
            });
        }
        completionBlock(_self);
    }];
    
    [self.connectedTerminalsDataTask resume];
}



- (void) reachabilityChanged:(NSObject *) notification {
    if ([self.status isEqualToString:kPEDStatusConnectingFailed]) {
        [self downloadConfig];
    }
    
    [super reachabilityChanged:notification];
}

- (NSString *) pedType {
    return kPEDTypeUSAEpay;
}

- (NSString *) pedName {
    return @"USAEpay";
}
- (NSString *) pedDescription {
    return self.selectedDevice ? [NSString stringWithFormat:@"%@", self.selectedDevice[@"name"]] : nil;
}

- (NSDictionary *) currencies {
    return @{}; // USD only
}

- (BOOL) canRefundFull {
    return true;    
}
- (BOOL) canRefundPartial {
    return true;
}

- (NSString *) endPoint {
    return self.sandbox ? @"https://sandbox.usaepay.com/api/v2" : @"https://usaepay.com/api/v2";
}

- (PEDPaymentRequest *) startPaymentInCurrency:(NSString*)currency forAmount:(NSDecimalNumber *)amount withReference:(NSString *)reference options:(NSDictionary *)options statusUpdateBlock:(void (^)(PEDPaymentRequest *))statusUpdateBlock {
    
    if (![currency isEqualToString:@"USD"]) {
        PEDPaymentRequest * paymentRequest = [[PEDPaymentRequest alloc] initWithConnection:self forAmount:amount inCurrency:currency withReference:reference statusUpdateBlock:statusUpdateBlock];
        
        paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk.usaepay.currency" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"USAEpay only supports USD, and this payment is in %@", paymentRequest.currency]}];
        paymentRequest.status = kPEDPaymentStatusInvalidAmount;
        return paymentRequest;
    } else {
        return [super startPaymentInCurrency:currency forAmount:amount withReference:reference options:options statusUpdateBlock:statusUpdateBlock];
    }
}

- (void) startPaymentRequest:(PEDPaymentRequest*)paymentRequest {
    paymentRequest.cancelRequested = false;
    paymentRequest.error = nil;
    paymentRequest.status = kPEDPaymentStatusInProgress;
    
    NSDictionary * poiRequest = @{
                                  @"devicekey": self.selectedDevice[@"id"],
                                  @"command": @"sale",
                                  @"amount": paymentRequest.amount,
                                  @"orderid": paymentRequest.reference,
                                  @"software": @"pedsdk"
                                  };
    
    PEDUSAEpayConnection * _self = self;
    self.paymentDataTask = [self usaepayDataTask:[NSString stringWithFormat:@"%@/paymentengine/payrequests", self.endPoint] method:@"POST" withRequest:poiRequest completionHander:^(NSData * _Nullable data, NSURLResponse * _Nullable zresponse, NSError * _Nullable error) {
        
        _self.paymentDataTask = nil;
        
        if (error) {
            paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Communication error: %@", error.localizedDescription]}];
            paymentRequest.status = kPEDPaymentStatusError;
        } else {
            NSDictionary * response = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            paymentRequest.gatewayReference = [response valueForKeyPath:@"key"];
            
            if (error) {
                paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Could not deserialise reponse: %@", error.localizedDescription]}];
                paymentRequest.status = kPEDPaymentStatusError;
            } else if ([response valueForKey:@"error"]) {
                paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[response valueForKeyPath:@"error"]}];
                paymentRequest.status = kPEDPaymentStatusError;
            } else {
                [self pollPaymentRequest:paymentRequest];
            }
        }
        
    }];
    
    [self.paymentDataTask resume];
}



-(void) pollPaymentRequest:(PEDPaymentRequest*)paymentRequest {
    PEDUSAEpayConnection * _self = self;
    self.pollDataTask = [self usaepayDataTask:[NSString stringWithFormat:@"%@/paymentengine/payrequests/%@", self.endPoint, paymentRequest.gatewayReference] method:@"GET" withRequest:nil completionHander:^(NSData * _Nullable data, NSURLResponse * _Nullable zresponse, NSError * _Nullable error) {

        _self.pollDataTask = nil;

        if (error) {
            paymentRequest.recoveryAttempt++;

            if ((int)paymentRequest.recoveryAttempt < 5) {
                [_self pollPaymentRequest:paymentRequest];
            } else {
                paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Communication error: %@", error.localizedDescription]}];
                paymentRequest.status = kPEDPaymentStatusError;
            }
        } else {
            NSDictionary * response = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];

            if (error) {
                paymentRequest.recoveryAttempt++;
                if ((int)paymentRequest.recoveryAttempt < 5) {
                    [_self pollPaymentRequest:paymentRequest];
                } else {
                        paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Could not deserialise reponse: %@", error.localizedDescription]}];
                    paymentRequest.status = kPEDPaymentStatusError;
                }
            } else {
                if ([[response valueForKeyPath:@"status"] isEqualToString:@"sending to device"] ||
                    [[response valueForKeyPath:@"status"] isEqualToString:@"sent to device"] ||
                    [[response valueForKeyPath:@"status"] isEqualToString:@"waiting for card dip"] ||
                    [[response valueForKeyPath:@"status"] isEqualToString:@"changing interfaces"] ||
                    [[response valueForKeyPath:@"status"] isEqualToString:@"customer see phone and tap again"] ||
                    [[response valueForKeyPath:@"status"] isEqualToString:@"processing payment"] ||
                    [[response valueForKeyPath:@"status"] isEqualToString:@"completing payment"] ||
                    [[response valueForKeyPath:@"status"] isEqualToString:@"capturing signature"] ||
                    [[response valueForKeyPath:@"status"] isEqualToString:@"signature capture error"] ||
                    [[response valueForKeyPath:@"status"] isEqualToString:@"customer see phone and tap again"] ||
                    [[response valueForKeyPath:@"status"] isEqualToString:@"sent to device"]) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [_self pollPaymentRequest:paymentRequest];
                    });
                } else if ([[response valueForKeyPath:@"status"] isEqualToString:@"timeout"]) {
                    paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:@"Timeout"}];
                    paymentRequest.status = kPEDPaymentStatusCancelled;
                } else if ([[response valueForKeyPath:@"status"] isEqualToString:@"error"]) {
                    paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:@"Error"}];
                    paymentRequest.status = kPEDPaymentStatusError;
                } else if ([[response valueForKeyPath:@"status"] isEqualToString:@"canceled"]) {
                    paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:@"Cancelled"}];
                    paymentRequest.status = kPEDPaymentStatusCancelled;
                } else if ([[response valueForKeyPath:@"status"] isEqualToString:@"transaction canceled"]) {
                    paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[response valueForKeyPath:@"transaction.error"]}];
                    paymentRequest.status = kPEDPaymentStatusCancelled;

                } else if (([[response valueForKeyPath:@"status"] isEqualToString:@"transaction complete"] && ([[response valueForKeyPath:@"transaction.result_code"] isEqualToString:@"E"] || [[response valueForKeyPath:@"transaction.result_code"] isEqualToString:@"D"])) || ([[response valueForKeyPath:@"status"] isEqualToString:@"transaction failed"])) {
                    paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[response valueForKeyPath:@"transaction.error"]}];
                    paymentRequest.status = kPEDPaymentStatusDeclined;

                } else if ([[response valueForKeyPath:@"status"] isEqualToString:@"transaction complete"] && [[response valueForKeyPath:@"transaction.result_code"] isEqualToString:@"A"]) {
                    paymentRequest.methodDescription = [response valueForKeyPath:@"transaction.creditcard.number"];
                    paymentRequest.authorisationCode = [response valueForKeyPath:@"transaction.authcode"];
                    paymentRequest.gatewayReference = [response valueForKeyPath:@"transaction.refnum"];
                    paymentRequest.paymentValues = @{
                                                     @"transactionId": paymentRequest.gatewayReference,
                                                     @"paymentMethodDetail": paymentRequest.methodDescription ? paymentRequest.methodDescription : [NSNull null],
                                                     
                                                     @"authCode": paymentRequest.authorisationCode ? paymentRequest.authorisationCode : [NSNull null]
                                                     };
                    paymentRequest.error = nil;
                    paymentRequest.status = kPEDPaymentStatusSuccess;

                } else {
                    NSLog(@"UNHANDLED RESPONSE");
                    paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Unhandled response"]}];
                    paymentRequest.status = kPEDPaymentStatusError;
                }
            }

        }
    }];

    [self.pollDataTask resume];
}



- (PEDRefundRequest *) refundPaymentReference:(NSString*)reference amount:(NSDecimalNumber *)amount currency:(NSString *)currency  withReason:(NSString *)reason statusUpdateBlock:(void(^)(PEDRefundRequest * request))statusUpdateBlock {
    
    PEDRefundRequest * refundRequest = [[PEDRefundRequest alloc] initWithConnection:self forTransactionReference:reference forAmount:amount inCurrency:currency withReason:reason statusUpdateBlock:statusUpdateBlock];
    
    if (!reference.length) {
        NSLog(@"Refund failed!: Reference Required.");
        refundRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Reference required."]}];
        refundRequest.status = kPEDPaymentStatusError;
    } else if (amount && ([amount isEqual:[NSDecimalNumber notANumber]] || (!amount.doubleValue))) {
        NSLog(@"Refund failed!: Invalid amount.");
        refundRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Invalid amount."]}];
        refundRequest.status = kPEDPaymentStatusError;
    } else {
        NSDictionary * poiRequest =
        @{
            @"command": @"refund",
            @"refnum": reference,
            @"amount": amount
        };
        
        PEDUSAEpayConnection * _self = self;
        self.refundDataTask = [self usaepayDataTask:[NSString stringWithFormat:@"%@/transactions", self.endPoint] method:@"POST" withRequest:poiRequest completionHander:^(NSData * _Nullable data, NSURLResponse * _Nullable zresponse, NSError * _Nullable error) {
            
            _self.refundDataTask = nil;
            
            if (error) {
                refundRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Communication error: %@", error.localizedDescription]}];
                refundRequest.status = kPEDPaymentStatusError;
            } else {
                NSDictionary * response = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                
                if (error) {
                    refundRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Could not deserialise reponse: %@", error.localizedDescription]}];
                    refundRequest.status = kPEDPaymentStatusError;
                } else if ([response valueForKey:@"error"]) {
                    refundRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[response valueForKeyPath:@"error"]}];
                    refundRequest.status = kPEDPaymentStatusError;
                } else if ([[response valueForKey:@"result_code"] isEqualToString:@"A"]) {
                    refundRequest.gatewayReference = [response valueForKeyPath:@"refnum"];
                    refundRequest.authorisationCode = [response valueForKey:@"authcode"];
                    refundRequest.error = nil;
                    refundRequest.status = kPEDPaymentStatusSuccess;
                } else {
                    refundRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[response valueForKeyPath:@"error"]}];
                    refundRequest.status = kPEDPaymentStatusDeclined;

                }
            }
        }];
        
        
        [self.refundDataTask resume];
    }
    
    return refundRequest;
}

- (void) cancelPayment:(PEDPaymentRequest *)paymentRequest {
    paymentRequest.cancelRequested = TRUE;
    
    PEDUSAEpayConnection * _self = self;
    self.cancelDataTask = [self usaepayDataTask:[NSString stringWithFormat:@"%@/paymentengine/payrequests/%@", self.endPoint, paymentRequest.gatewayReference] method:@"DELETE" withRequest:nil completionHander:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        _self.cancelDataTask = nil;
    }];
    
    [self.cancelDataTask resume];
}


- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    return false;
}

- (NSArray *) deviceList {
    NSMutableArray *devices = [NSMutableArray array];
    for (NSDictionary * device in self.connectedTerminals) {
        [devices addObject:@{
                             @"id": [device valueForKeyPath:@"key"],
                             @"name": [NSString stringWithFormat:@"%@ (%@ #%@)", [device valueForKeyPath:@"name"], [device valueForKeyPath:@"terminal_info.model"], [device valueForKeyPath:@"terminal_info.serial"]],
                             @"status": [device valueForKeyPath:@"status"]
                             }];
    }
    return devices;
}

@end

