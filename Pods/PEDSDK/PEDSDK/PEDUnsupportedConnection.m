//
//  PEDUnsupportedConnection.m
//  PEDSDK
//
//  Created by Phil Peters on 19/09/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import "PEDUnsupportedConnection.h"
#import "PEDConnection+Private.h"
#import "PEDPaymentRequest+Private.h"

@implementation PEDUnsupportedConnection

- (id) initWithConfiguration:(NSDictionary *)configuration statusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock {
    self = [super initWithConfiguration:configuration statusUpdateBlock:statusUpdateBlock];
    
    self.status = kPEDStatusConnecting;
    
    PEDUnsupportedConnection * _self = self;
    dispatch_async( dispatch_get_main_queue(), ^{
        _self.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"This payment method is not supported by this version of the SDK."]}];
        _self.status = kPEDStatusConnectingFailed;

        statusUpdateBlock(_self);
    });
    
    return self;
}

- (NSString *) pedType {
    return kPEDTypeiZettle;
}

- (NSString *) pedName {
    return @"iZettle";
}
- (NSString *) pedDescription {
    return @"This payment method is not supported";
}
- (NSDictionary *) currencies {
    return @{
             @"GBP": @{
                     @"methods": @[kPEDCardTypeMastercard, kPEDCardTypeVisa, kPEDCardTypeAMEX,
                                   kPEDCardTypeMaestro, kPEDCardTypeVPay, kPEDCardTypeVisaElectron,
                                   kPEDCardTypeJCB, kPEDCardTypeDiners],
                     @"minCharge": [NSDecimalNumber decimalNumberWithString:@"1"],
                     @"maxCharge": [NSDecimalNumber decimalNumberWithString:@"5000"]
                     }
             };
}

- (BOOL) canRefundFull {
    return true;
}
- (BOOL) canRefundPartial {
    return false;
}

- (UIViewController*) topMostController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

- (PEDPaymentRequest *) startPaymentInCurrency:(NSString*)currency forAmount:(NSDecimalNumber *)amount withReference:(NSString *)reference options:(NSDictionary *)options statusUpdateBlock:(void (^)(PEDPaymentRequest *))statusUpdateBlock {
    @throw [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Unsupported Method."]}];
}

- (void) cancelPayment:(PEDPaymentRequest *)payment {
    @throw [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Unsupported Method."]}];
}

@end
