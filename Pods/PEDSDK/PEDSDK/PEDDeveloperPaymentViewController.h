//
//  PEDDeveloperViewController.h
//  PEDSDK
//
//  Created by Phil Peters on 25/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PEDPaymentRequest;

@interface PEDDeveloperPaymentViewController : UIViewController

- (id) initWithRequest:(PEDPaymentRequest *)request andCallback:(void(^)(PEDPaymentRequest * request))callback;

@end
