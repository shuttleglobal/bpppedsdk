//
//  PEDDeveloperViewController.m
//  PEDSDK
//
//  Created by Phil Peters on 25/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import "PEDDeveloperPaymentViewController.h"
#import "PEDConnection.h"
#import "PEDPaymentRequest.h"
#import "PEDPaymentRequest+Private.h"

@interface PEDDeveloperPaymentViewController ()
@property (nonatomic, retain) PEDPaymentRequest * request;
@property (nonatomic, copy) void (^paymentCallbackBlock)(PEDPaymentRequest * request);

@property (nonatomic, retain) UIButton * cancelButton;
@property (nonatomic, retain) UIButton * errorButton;
@property (nonatomic, retain) UIButton * successButton;
@end

@implementation PEDDeveloperPaymentViewController

- (id) initWithRequest:(PEDPaymentRequest *)request andCallback:(void(^)(PEDPaymentRequest * request))callback {
    self = [self init];
    self.request = request;
    self.paymentCallbackBlock = callback;

    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.cancelButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.cancelButton.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
    [self.cancelButton setTitle:@"Emulate Cancel" forState:UIControlStateNormal];
    [self.cancelButton addTarget:self action:@selector(cancelClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.cancelButton];
    
    self.errorButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.errorButton.backgroundColor = [UIColor redColor];
    [self.errorButton setTitle:@"Emulate Error" forState:UIControlStateNormal];
    [self.errorButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.errorButton addTarget:self action:@selector(errorClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.errorButton];

    self.successButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.successButton.backgroundColor = [UIColor greenColor];
    [self.successButton setTitle:@"Emulate Approved" forState:UIControlStateNormal];
    [self.successButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.successButton addTarget:self action:@selector(successClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.successButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillLayoutSubviews {
    self.cancelButton.frame = CGRectMake(self.view.frame.size.width / 2 - 150, 100, 300, 40);
    self.errorButton.frame = CGRectMake(self.view.frame.size.width / 2 - 150, 160, 300, 40);
    self.successButton.frame = CGRectMake(self.view.frame.size.width / 2 - 150, 220, 300, 40);
}

- (void) cancelClick:(id)sender {
    PEDDeveloperPaymentViewController * _self = self;

    [self dismissViewControllerAnimated:true completion:^{
        _self.request.error = nil;
        _self.request.status = kPEDPaymentStatusCancelled;
    }];
}

- (void) errorClick:(id)sender {
    PEDDeveloperPaymentViewController * _self = self;

    [self dismissViewControllerAnimated:true completion:^{
        _self.request.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:kPEDPaymentErrorGeneral userInfo:@{NSLocalizedDescriptionKey: @"Developer test exception."}];
        _self.request.status = kPEDPaymentStatusError;
    }];
}

- (void) successClick:(id)sender {
    PEDDeveloperPaymentViewController * _self = self;

    [self dismissViewControllerAnimated:true completion:^{
        _self.request.methodType = kPEDCardTypeVisa;
        _self.request.methodDescription = @"1234";
        _self.request.gatewayReference = @"TEST_1234567890";
        
        _self.request.error = nil;
        _self.request.status = kPEDPaymentStatusSuccess;
    }];
}

@end
