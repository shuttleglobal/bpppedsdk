//
//  PEDConnection+Private.h
//  pedsdk
//
//  Created by Phil Peters on 22/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#ifndef PEDConnection_Private_h
#define PEDConnection_Private_h

@class PEDReachability;

@interface PEDConnection()

@property (nonatomic, retain) PEDReachability * reachability;

@property (nonatomic, retain) NSDictionary * configuration;
@property (nonatomic, copy) void (^statusUpdateBlock)(PEDConnection *conn);

@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSError * error;

- (id) initWithConfiguration:(NSDictionary *)configuration statusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock;
- (void) reachabilityChanged:(NSObject *) notification;

@end

#endif /* PEDConnection_Private_h */
