//
//  PEDWhiteLabelledConnection.h
//  PEDSDK
//
//  Created by Phil Peters on 11/03/2019.
//  Copyright © 2019 Pay With Bolt. All rights reserved.
//

#import "PEDConnection.h"


NS_ASSUME_NONNULL_BEGIN

@class PEDPaymentViewController;

@interface PEDWhiteLabelledConnection : PEDConnection

@property (nonatomic, retain) UINavigationController * navigationViewController;
@property (nonatomic, retain) PEDPaymentViewController * paymentViewController;

-(void) refreshDeviceListOnCompletion:(void (^)(PEDWhiteLabelledConnection *)) completionBlock;

- (NSArray *) deviceList;
- (NSDictionary *) selectedDevice;
- (void) setSelectedDevice:(NSDictionary *)deviceId;

- (void) setSelectedDevice:(NSDictionary *)deviceId duringActivePaymentRequest:(PEDPaymentRequest *) paymentRequest;

- (void) startPaymentRequest:(PEDPaymentRequest*)paymentRequest; // Override in subclass

@end

NS_ASSUME_NONNULL_END
