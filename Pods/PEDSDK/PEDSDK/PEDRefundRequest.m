//
//  PEDRefundRequest.m
//  pedsdk
//
//  Created by Phil Peters on 22/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import "PEDRefundRequest.h"
#import "PEDRefundRequest+Private.h"

@interface PEDRefundRequest()
@property (nonatomic, retain) NSMutableDictionary * privateValues;
@end

@implementation PEDRefundRequest

- (void) setStatus:(NSString *)status {
    _status = status;
    
    if (self.statusUpdateBlock) {
        PEDRefundRequest * _self = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            _self.statusUpdateBlock(_self);
        });
    }
}

- (void) updateNonce {
    NSInteger txnCounter = [[NSUserDefaults standardUserDefaults] integerForKey:@"PEDSDKRefundRequestCounter"];
    [[NSUserDefaults standardUserDefaults] setInteger:txnCounter + 1 forKey:@"PEDSDKRefundRequestCounter"];
    _nonce = [NSString stringWithFormat:@"R%li", txnCounter];
}

- (id) initWithConnection:(PEDConnection *) connection forTransactionReference:(NSString *)reference forAmount:(NSDecimalNumber *) amount inCurrency:(NSString *)currency withReason:(NSString *)reason statusUpdateBlock:(void(^)(PEDRefundRequest * request))statusUpdateBlock {
    self = [super init];
    _connection = connection;
    _amount = amount;
    _currency = currency;
    _reference = reference;
    _reason = reason;
    self.statusUpdateBlock = statusUpdateBlock;
    self.privateValues = [NSMutableDictionary dictionary];
    [self updateNonce];
    return self;
}


@end
