//
//  PEDRefundRequest.h
//  pedsdk
//
//  Created by Phil Peters on 22/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kPEDPaymentStatusInProgress @"INPROGRESS"
#define kPEDPaymentStatusCancelled @"CANCELLED"
#define kPEDPaymentStatusError @"ERROR"
#define kPEDPaymentStatusPreapproved @"PREAPPROVED"
#define kPEDPaymentStatusSuccess @"SUCCESS"
#define kPEDPaymentStatusDeclined @"DECLINED"
#define kPEDPaymentStatusInvalidAmount @"INVALIDAMOUNT"

@class PEDConnection;

@interface PEDRefundRequest : NSObject

@property (nonatomic, readonly) PEDConnection * connection;
@property (nonatomic, readonly) NSString * reference; // Original transaction
@property (nonatomic, readonly) NSString * reason;
@property (nonatomic, readonly) NSString * currency;
@property (nonatomic, readonly) NSDecimalNumber * amount;

@property (nonatomic, readonly) NSString * status;
//@property (nonatomic, readonly) NSString * methodType; // TO POPULATE
//@property (nonatomic, readonly) NSString * methodDescription; // TO POPULATE
//@property (nonatomic, readonly) NSString * gatewayReference; // TO POPULATE
//@property (nonatomic, readonly) NSString * boltStatus; // TO POPULATE
//@property (nonatomic, readonly) NSString * boltReceipt; // TO POPULATE
@property (nonatomic, readonly) NSError * error;

- (id) initWithConnection:(PEDConnection *) connection forTransactionReference:(NSString *)reference forAmount:(NSDecimalNumber *) amount inCurrency:(NSString *)currency withReason:(NSString *)reason statusUpdateBlock:(void(^)(PEDRefundRequest * request))statusUpdateBlock;



@end
