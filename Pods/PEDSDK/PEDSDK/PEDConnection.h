//
//  PEDConnection.h
//  pedsdk
//
//  Created by Phil Peters on 22/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PEDConfigDelegate.h"
#import <UIKit/UIKit.h>

@class PEDPaymentRequest, PEDRefundRequest;

#define kPEDTypeAdyen @"ADYEN"
#define kPEDTypeAuthNet @"AUTHNET"
#define kPEDTypeiZettle @"IZETTLE"
#define kPEDTypeMoneris @"MONERIS"
#define kPEDTypeUSAEpay @"USAEPAY"
#define kPEDTypeSquare @"SQUARE"
#define kPEDTypeUnsupported @"UNSUPPORTED"
#define kPEDTypeDeveloper @"DEVELOPER"

#define kPEDStatusConnecting @"CONNECTING" // Connecting to Bolt
#define kPEDStatusConnectingFailed @"CONNECTION_FAILED" // Connecting to Bolt
#define kPEDStatusErrorSDK @"ERROR_SDK" // Error connecting to Bolt
#define kPEDStatusConnectedOffline @"CONNECTED_OFFLINE" // Connected to Bolt, but internet offline so no payments (disable button)
#define kPEDStatusConnectedNoPed @"CONNECTED_NO_PED" // Connected to Bolt, but PED Device not ready (enable button)
#define kPEDStatusConnectedUpgrade @"CONNECTED_UPGRADE" // Connected to Bolt & PED, but PED requires upgrade (?)
#define kPEDStatusConnectedReady @"CONNECTED_READY" // Connected to Bolt & PED, everything OK
#define kPEDStatusConnectedUpgrading @"CONNECTED_UPGRADING" // Connected to Bolt & PED and PED is midway through a firmware upgrade

#define kPEDCardTypeMastercard @"MASTERCARD"
#define kPEDCardTypeVisa @"VISA"
#define kPEDCardTypeAMEX @"AMEX"
#define kPEDCardTypeMaestro @"MAESTRO"
#define kPEDCardTypeVPay @"VPAY"
#define kPEDCardTypeVisaElectron @"ELECTRON"
#define kPEDCardTypeJCB @"JCB"
#define kPEDCardTypeDiners @"DINERS"

#define kPEDCancelPaymentErrorUnsupported @"UNSUPPORTED"
#define kPEDCancelPaymentErrorProcessing @"PROCESSING"

typedef NS_ENUM(NSUInteger, PEDConnectionError) {
    kPEDConnectionErrorInvalidPedType, // A Ped Type not supported by this version SDK
    kPEDConnectionErrorInvalidPedImplemention,
    kPEDConnectionErrorInvalidAccount};

@interface PEDConnection : NSObject

// Test Connection: Developer
// ----------------------------
// Show a Fake Screen, rather than require an PED

+ (id) developerTestConnectionWithStatusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate;


+ (id) adyenTestConnectionWithStatusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate;

// Test Connection: iZettle
// ------------------------
// This will accept any card, but wont take money from the account
//
// Login using:
//      Email: externaldemo@izettle.com
//      Password: externaldemo123
//
// NOTE: iZettle use requires a plist value for "iZettleAPIKey" that is coded against the app's bundle ID

+ (id) iZettleTestConnectionWithStatusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate;

+ (id) usaepayTestConnectionWithStatusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate;

+ (id) monerisTestConnectionWithStatusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate;

+ (id) PEDDemoConnectionWithType:(NSString *)type statusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate;


// New PED Connection
// -------------------
//
// Pass in:
//      configuration: Unique per PED type
//      statusUpdateBlock: This will be fired when the PED is ready or loses internet connectivity

+ (id) PEDConnectionWithConfiguration:(NSDictionary *)configuration statusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate;

- (NSString *) pedType;
- (NSString *) pedName;
- (NSString *) pedDescription;
- (NSDictionary *) currencies;
- (BOOL) canRefundFull;
- (BOOL) canRefundPartial;
- (BOOL) hideRefreshDeviceList;

- (NSString *) status;      // DEFINE kPEDStatusXXX
- (NSError *) error;        // If status == kPEDStatusErrorSDK

- (PEDPaymentRequest *) startPaymentInCurrency:(NSString*)currency forAmount:(NSDecimalNumber *)amount withReference:(NSString *)reference options:(NSDictionary *)options statusUpdateBlock:(void(^)(PEDPaymentRequest * request))statusUpdateBlock;

- (void) cancelPayment:(PEDPaymentRequest *)payment;

- (PEDRefundRequest *) refundPaymentReference:(NSString*)reference amount:(NSDecimalNumber *)amount currency:(NSString *)currency withReason:(NSString *)reason statusUpdateBlock:(void(^)(PEDRefundRequest * request))statusUpdateBlock;

- (void) updateFirmwareOnComplete:(void(^)(BOOL success, NSString *message))completionHandler;


@end
