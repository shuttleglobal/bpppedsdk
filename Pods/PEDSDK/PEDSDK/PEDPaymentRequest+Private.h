//
//  PEDPaymentRequest+Private.h
//  pedsdk
//
//  Created by Phil Peters on 23/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#ifndef PEDPaymentRequest_Private_h
#define PEDPaymentRequest_Private_h
#import "PEDPaymentRequest.h"

@interface PEDPaymentRequest()

@property (nonatomic, copy) void (^statusUpdateBlock)(PEDPaymentRequest *request);

@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * methodType;
@property (nonatomic, retain) NSString * methodDescription;
@property (nonatomic, retain) NSString * authorisationCode;
@property (nonatomic, retain) NSString * gatewayReference;
@property (nonatomic, retain) NSString * customerId;
@property (nonatomic, assign) BOOL cancelRequested;
@property (nonatomic, retain) NSDictionary * paymentValues;
@property (nonatomic, readonly) NSString * nonce;
@property (nonatomic, retain) NSError * error;
@property (nonatomic, assign) NSInteger * recoveryAttempt; 

@end


#endif /* PEDPaymentRequest_Private_h */
