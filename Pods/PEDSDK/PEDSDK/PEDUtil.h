//
//  PEDUtil.h
//  PEDSDK
//
//  Created by Phil Peters on 30/01/2019.
//  Copyright © 2019 Pay With Bolt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PEDUtil : NSObject

+ (UIViewController*) topMostController;
+ (NSString *)MD5:(NSString *)string;
+ (void) alertWithTitle:(NSString *)title message:(NSString *)message;

@end

NS_ASSUME_NONNULL_END
