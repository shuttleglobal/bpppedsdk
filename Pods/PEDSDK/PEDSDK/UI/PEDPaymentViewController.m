//
//  PEDPaymentViewController.m
//  PEDSDK
//
//  Created by Phil Peters on 06/03/2019.
//  Copyright © 2019 Pay With Bolt. All rights reserved.
//

#import "PEDPaymentViewController.h"
#import "PEDPaymentRequest.h"
#import "PEDPaymentRequest+Private.h"
#import "PEDWhiteLabelledConnection.h"

@interface PEDPaymentViewController ()

@property (nonatomic, copy) void (^statusUpdateBlock)(PEDPaymentRequest *request);

@property (nonatomic, retain) PEDWhiteLabelledConnection * connection;
@property (nonatomic, retain) PEDPaymentRequest * paymentRequest;

@property (nonatomic, assign) BOOL cancelled;
@property (nonatomic, assign) BOOL declined;
@property (nonatomic, assign) BOOL approved;
@property (nonatomic, retain) NSString * activityTitle;
@property (nonatomic, retain) NSString * statusTitle;
@property (nonatomic, retain) NSString * statusSubTitle;

// View Objects
@property (nonatomic, retain) UIView * headerView;

@property (nonatomic, retain) UIActivityIndicatorView * activitySpinner;
@property (nonatomic, retain) UILabel * activityLabel;
@property (nonatomic, retain) UILabel * statusLabel;
@property (nonatomic, retain) UILabel * statusSubLabel;

@property (nonatomic, retain) UIBarButtonItem * cancelButton;
@property (nonatomic, retain) UIBarButtonItem * setupButton;
@property (nonatomic, retain) UIBarButtonItem * refreshButton;
@property (nonatomic, retain) UIActivityIndicatorView * refreshSpinner;

@property (nonatomic, assign) BOOL setup;

@end

@implementation PEDPaymentViewController

- (id) initWithPaymentRequest:(PEDPaymentRequest *) paymentRequest forConnection:(PEDWhiteLabelledConnection *) connection {
    NSLog(@"PEDPaymentViewController.initWithPaymentRequest");
    self = [self init];
    self.paymentRequest = paymentRequest;
    self.connection = connection;
    
    // Overwrite statusUpdateBlock with one that can block during modal dismiss
    self.statusUpdateBlock = paymentRequest.statusUpdateBlock;
    PEDPaymentViewController * _self = self;
    self.paymentRequest.statusUpdateBlock = ^(PEDPaymentRequest *request) {
        [_self paymentRequestStatusUpdate:request];
    };
    
    return self;
}

// UI Below here
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    
    self.headerView = [[UIView alloc] init];
    [self.view addSubview:self.headerView];
    
    self.statusLabel = [[UILabel alloc] init];
    self.statusLabel.font = [UIFont systemFontOfSize:32 weight:UIFontWeightMedium];
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    [self.headerView addSubview:self.statusLabel];
    
    self.statusSubLabel = [[UILabel alloc] init];
    self.statusSubLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightThin];
    self.statusSubLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
    self.statusSubLabel.textAlignment = NSTextAlignmentCenter;
    [self.headerView addSubview:self.statusSubLabel];

    self.activitySpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.headerView addSubview:self.activitySpinner];

    self.activityLabel = [[UILabel alloc] init];
    self.activityLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightThin];
    self.activityLabel.textAlignment = NSTextAlignmentCenter;
    self.activityLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
    [self.headerView addSubview:self.activityLabel];

    self.cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonClick)];
    self.navigationItem.leftBarButtonItem = self.cancelButton;

    self.setupButton = [[UIBarButtonItem alloc] initWithTitle:@"Setup" style:UIBarButtonItemStylePlain target:self action:@selector(setupButtonClick)];
    self.navigationItem.rightBarButtonItem = self.setupButton;
    
    self.refreshButton = [[UIBarButtonItem alloc] initWithTitle:@"Refresh" style:UIBarButtonItemStylePlain target:self action:@selector(refreshButtonClick)];
    self.refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.refreshSpinner startAnimating];
    
    if (!self.connection.selectedDevice) {
        [self setupButtonClick];
    }
}

- (void) viewWillLayoutSubviews {
    self.cancelled = false;
    self.declined = false;
    self.approved = false;
    self.activityTitle = @"";
    self.statusTitle = @"";
    self.statusSubTitle = @"";
    
    self.title = @"Card Payment";
    
    NSLog(@"viewWillLayoutSubviews: %@", self.paymentRequest.status);
    
    if ([self.paymentRequest.status isEqualToString:kPEDPaymentStatusInProgress]) {
        NSNumberFormatter* nf = [[NSNumberFormatter alloc] init];
        nf.usesGroupingSeparator = true;
        nf.numberStyle = NSNumberFormatterCurrencyStyle;
        nf.locale = [[NSLocale alloc] initWithLocaleIdentifier:self.paymentRequest.currency];
        nf.currencyCode  =self.paymentRequest.currency;
        NSString* amount = [nf stringFromNumber: self.paymentRequest.amount];
        
        self.statusTitle = [NSString stringWithFormat:@"%@", amount];
        self.statusSubTitle = @"Tap / Insert Card";
        
        if (self.paymentRequest.cancelRequested) {
            self.activityTitle = @"Cancelling";
        }
    } else if ([self.paymentRequest.status isEqualToString:kPEDPaymentStatusCancelled]) {
        self.statusTitle = @"Cancelled";
        self.statusSubTitle = self.paymentRequest.error.localizedDescription;
        self.cancelled = TRUE;
    } else if ([self.paymentRequest.status isEqualToString:kPEDPaymentStatusError]) {
        self.statusTitle = @"Sorry!";
        self.statusSubTitle = self.paymentRequest.error.localizedDescription;
        self.cancelled = TRUE;
    } else if ([self.paymentRequest.status isEqualToString:kPEDPaymentStatusDeclined]) {
        self.statusTitle = @"Declined";
        self.statusSubTitle = self.paymentRequest.error.localizedDescription;
        self.declined = TRUE;
    } else if ([self.paymentRequest.status isEqualToString:kPEDPaymentStatusSuccess]) {
        self.statusTitle = @"Approved";
        self.statusSubTitle = [NSString stringWithFormat:@"Authorization code: %@", self.paymentRequest.authorisationCode];
        self.approved = TRUE;
    }
    
    if (self.activityTitle.length) {
        [self.activitySpinner startAnimating];
    } else {
        [self.activitySpinner stopAnimating];
    }

    self.statusLabel.textColor = self.approved ? [UIColor colorWithRed:65/255.0 green:117/255.0 blue:5/255.0 alpha:1] : self.declined ? [UIColor colorWithRed:208/255.0 green:2/255.0 blue:27/255.0 alpha:1] : self.cancelled ? [UIColor colorWithWhite:0.4 alpha:1] : [UIColor blackColor];
    self.activityLabel.text = self.activityTitle;
    self.statusLabel.text = self.statusTitle;
    self.statusSubLabel.text = self.statusSubTitle;
    
}
- (void) viewDidLayoutSubviews {
    CGFloat spacing = 15;
    CGFloat width = UIEdgeInsetsInsetRect(self.view.bounds, self.view.safeAreaInsets).size.width;
    
    self.statusLabel.frame = CGRectMake(spacing, spacing, width - 2 * spacing, self.statusLabel.text.length ? self.statusLabel.font.lineHeight : 0);
    self.statusSubLabel.frame = CGRectMake(spacing, CGRectGetMaxY(self.statusLabel.frame), width - 2 * spacing, self.statusSubLabel.text.length ? self.statusSubLabel.font.lineHeight : 0);
    self.activityLabel.frame = CGRectMake(21 + spacing, CGRectGetMaxY(self.statusSubLabel.frame) + (self.activityLabel.text.length ? spacing + 2 : 0), width - 21 - spacing * 2, self.activityLabel.text.length ? self.activityLabel.font.lineHeight : 0);
    self.activitySpinner.frame = CGRectMake((width - self.activityLabel.intrinsicContentSize.width) / 2 - 16, CGRectGetMaxY(self.statusSubLabel.frame) + spacing, 21, 21);

    self.headerView.frame = CGRectMake(0, 0, width, CGRectGetMaxY(self.activityLabel.frame) + spacing);
    
    if (self.tableView.tableHeaderView.frame.size.height != self.headerView.frame.size.height) {
        self.tableView.tableHeaderView = self.headerView;
    }
    NSLog(@"preferredContentSize: %@", NSStringFromCGSize(self.tableView.contentSize));
    self.preferredContentSize = self.tableView.contentSize;
}

- (void) paymentRequestStatusUpdate:(PEDPaymentRequest *) paymentRequest {
    NSLog(@"paymentRequestStatusUpdate: %@", paymentRequest.status);
    [self.view setNeedsLayout];
    //Reload
    [self.tableView reloadData];
    
    float dismiss = -1;
    if ([self.paymentRequest.status isEqualToString:kPEDPaymentStatusCancelled]) {
        dismiss = self.paymentRequest.cancelRequested ? 0 : 1;
    } else if ([self.paymentRequest.status isEqualToString:kPEDPaymentStatusDeclined]) {
        dismiss = 2;
    } else if ([self.paymentRequest.status isEqualToString:kPEDPaymentStatusSuccess]) {
        dismiss = 2;
    }
    
    if (dismiss >= 0) {
        PEDPaymentViewController * _self = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, dismiss * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            if (_self.parentViewController) {
                [_self dismissViewControllerAnimated:TRUE completion:^{
                    _self.statusUpdateBlock(paymentRequest);
                }];
            }
        });
    } else {
        if (![paymentRequest.status isEqualToString:kPEDPaymentStatusError]) {
            self.statusUpdateBlock(paymentRequest);
        }
    }
}


- (void) cancelButtonClick {
    if ([self.paymentRequest.status isEqualToString:kPEDPaymentStatusInProgress]) {
        [self.connection cancelPayment:self.paymentRequest];
        [self.view setNeedsLayout];
    } else {
        self.paymentRequest.cancelRequested = true;
        self.paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:@"Cancelled"}];
        self.paymentRequest.status = kPEDPaymentStatusCancelled;
    }
}


- (void) setupButtonClick {
    self.setup = !self.setup;
    self.navigationItem.rightBarButtonItem = self.connection.hideRefreshDeviceList ? nil : self.refreshButton;
    
    [self.tableView reloadData];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.setup ? 1 : 0;
}

-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Select Card Reader";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.connection.deviceList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reuseIdentifier"];
    }
    
    NSDictionary * device = [self.connection.deviceList objectAtIndex:indexPath.row];
    
    cell.textLabel.text = device[@"name"];
    cell.accessoryType =self.connection.selectedDevice &&  [device[@"id"] isEqual:self.connection.selectedDevice[@"id"]] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary * device = [self.connection.deviceList objectAtIndex:indexPath.row];
    
    [self.connection setSelectedDevice:device duringActivePaymentRequest:self.paymentRequest];
}


-(void) refreshButtonClick {
    PEDPaymentViewController * _self = self;
    [self.connection refreshDeviceListOnCompletion:^(PEDWhiteLabelledConnection * _Nonnull connection) {
        dispatch_async( dispatch_get_main_queue(), ^{
            _self.navigationItem.rightBarButtonItem = _self.refreshButton;
        });
    }];
    

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.refreshSpinner];
}

@end
