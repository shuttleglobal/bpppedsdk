//
//  PEDSquareConnection.m
//  pedsdk
//
//  Created by Phil Peters on 22/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import "PEDSquareConnection.h"
#import "PEDConnection+Private.h"
#import "PEDPaymentRequest.h"
#import "PEDPaymentRequest+Private.h"
#import "PEDRefundRequest.h"
#import "PEDRefundRequest+Private.h"
#import "PEDConfigKey.h"
#import "PEDUtil.h"
#import <SquareReaderSDK/SquareReaderSDK.h>
#import <CoreLocation/CoreLocation.h>
#import <AVKit/AVKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface PEDSquareConnection()<SQRDCheckoutControllerDelegate, CLLocationManagerDelegate, SQRDReaderSettingsControllerDelegate, CBCentralManagerDelegate>
@property (nonatomic, retain) PEDPaymentRequest * currentRequest;
@property (nonatomic, retain) CLLocationManager * locationManager;
@property (nonatomic, retain) CBCentralManager * bluetoothManager;
@property (nonatomic, retain) NSString * mobileKey;
@property (nonatomic, retain) NSString * apiKey;
@property (nonatomic, retain) NSString * client;
@property (nonatomic, retain) NSString * integrationName;
@property (nonatomic, assign) BOOL skipReceipt;
@property (nonatomic, retain) NSURLSessionDataTask * refundDataTask;
@property (nonatomic, assign) BOOL debug;
@property (nonatomic, assign) BOOL bluetoothDisabled;
@property (nonatomic, assign) BOOL uxPresented;
@property (nonatomic, copy) void (^firmwareCompletionHandler)(BOOL, NSString *);
@end

@implementation PEDSquareConnection

- (id) initWithConfiguration:(NSDictionary *)configuration statusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate {
    self = [super initWithConfiguration:configuration statusUpdateBlock:statusUpdateBlock];
    self.debug = true;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [SQRDReaderSDK initializeWithApplicationLaunchOptions:@{}];
    
    [self.locationManager requestWhenInUseAuthorization];

    self.bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
    [self centralManagerDidUpdateState:self.bluetoothManager];
    
    return self;
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        [[[CLLocationManager alloc] init] requestWhenInUseAuthorization];
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        if (![self.status isEqualToString:kPEDStatusConnectingFailed]) {
            [PEDUtil alertWithTitle:@"Sorry!" message:@"To use Square you must go to General Settings and enable location services"];
            self.status = kPEDStatusConnectingFailed;
            self.statusUpdateBlock(self);
        }
    } else {
        [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
            if (!granted && ![self.status isEqualToString:kPEDStatusConnectingFailed]) {
                [PEDUtil alertWithTitle:@"Sorry!" message:@"To use Square you must go to General Settings and enable record permissions"];
                self.status = kPEDStatusConnectingFailed;
                self.statusUpdateBlock(self);
            } else if (granted) {
                [self downloadConfig];
            }
        }];
    }
}

- (void) downloadConfig {
    if (!NSThread.isMainThread) {
        id _self = self;
        dispatch_async( dispatch_get_main_queue(), ^{
            [_self downloadConfig];
        });
        return;
    }
    
    if (!self.status || [self.status isEqualToString:kPEDStatusConnectingFailed]) {
        PEDSquareConnection * _self = self;

        //  If authorised against a different code, deauthorise
        if ([SQRDReaderSDK sharedSDK].isAuthorized && !self.uxPresented){
            [[SQRDReaderSDK sharedSDK] deauthorizeWithCompletionHandler:^(NSError * _Nullable error) {
                [_self downloadConfig];
                _self.uxPresented = false;
            }];
            self.uxPresented = true;
            return;
        }

        self.status = kPEDStatusConnecting;
        self.statusUpdateBlock(self);

        
        BOOL debug = false;
        if (debug) {
            self.debug = true;
            NSDictionary * config = @{
                @"authorization_code": @"",
                @"access_token": @"",
                @"client": @"",
                @"integration_name": @""
            };

           [[SQRDReaderSDK sharedSDK] authorizeWithCode:config[@"authorization_code"] completionHandler:^(SQRDLocation * _Nullable location, NSError * _Nullable error) {
                      if (error) {
                          _self.status = kPEDStatusConnectingFailed;
                          _self.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Could not authorise with Square"]}];
                      } else {
                          self.apiKey = config[@"access_token"];
                          self.mobileKey = config[@"authorization_code"];
                          self.client = config[@"client"];
                          self.integrationName = config[@"integration_name"];
                          self.skipReceipt = config[@"skip_receipt"];
                          if (SQRDReaderSDK.sharedSDK.authorizedLocation.isCardProcessingActivated) {
                              dispatch_async( dispatch_get_main_queue(), ^{
                                  NSLog(@"SQRDReaderSettingsController");
                                  SQRDReaderSettingsController * vc = [[SQRDReaderSettingsController alloc] initWithDelegate:self];
                                  [vc presentFromViewController:[PEDUtil topMostController]];

                                  _self.status = kPEDStatusConnectedReady;
                                  _self.statusUpdateBlock(_self);
                              });
                          } else {
                              [PEDUtil alertWithTitle:@"Sorry!" message:@"The authorized Square location is not activated for card processing."];
                              _self.status = kPEDStatusConnectingFailed;
                          }
                      }
                      _self.statusUpdateBlock(_self);
                  }];

            return;
        }
        
        NSURL * url = [NSURL URLWithString:[self.configuration valueForKeyPath:@"pedValues.configuration"]];
        
        NSLog(@"PEDSquareConnection.downloadConfiguration: %@", url.absoluteString);
        [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (error) {
                // Dont call callback because we can't handle an error without a PEDConnection
                // On network status change we should try to connect again
                _self.status = kPEDStatusConnectingFailed;
                _self.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Could not connect to server to retrieve credentials"]}];
            } else {
                NSDictionary * config = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                _self.configuration = config;

                dispatch_async( dispatch_get_main_queue(), ^{
                     [[SQRDReaderSDK sharedSDK] authorizeWithCode:config[@"authorization_code"] completionHandler:^(SQRDLocation * _Nullable location, NSError * _Nullable error) {
                           if (error) {
                               _self.status = kPEDStatusConnectingFailed;
                               _self.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Could not authorise with Square"]}];
                           } else {
                               self.apiKey = config[@"access_token"];
                               self.mobileKey = config[@"authorization_code"];
                               self.client = config[@"client"];
                               self.integrationName = config[@"integration_name"];
                               self.skipReceipt = config[@"skip_receipt"];
                               if (SQRDReaderSDK.sharedSDK.authorizedLocation.isCardProcessingActivated) {
                                   dispatch_async( dispatch_get_main_queue(), ^{
                                       NSLog(@"SQRDReaderSettingsController");
                                       SQRDReaderSettingsController * vc = [[SQRDReaderSettingsController alloc] initWithDelegate:self];
                                       [vc presentFromViewController:[PEDUtil topMostController]];
                                       
                                       _self.status = kPEDStatusConnectedReady;
                                       _self.statusUpdateBlock(_self);
                                   });
                               } else {
                                   [PEDUtil alertWithTitle:@"Sorry!" message:@"The authorized Square location is not activated for card processing."];
                                   _self.status = kPEDStatusConnectingFailed;
                               }
                           }
                           _self.statusUpdateBlock(_self);
                       }];
                });
            }
        }] resume];

    }
}

- (void) reachabilityChanged:(NSObject *) notification {
    if ([self.status isEqualToString:kPEDStatusConnectingFailed]) {
        [self downloadConfig];
    }
    
    [super reachabilityChanged:notification];
}

- (NSString *) pedType {
    return kPEDTypeSquare;
}

- (NSString *) pedName {
    return @"Square";
}
- (NSString *) pedDescription {
    return [NSString stringWithFormat:@"%@", self.pedName];
}

- (BOOL) canRefundFull {
    return true;
}
- (BOOL) canRefundPartial {
    return true;
}

- (PEDPaymentRequest *) startPaymentInCurrency:(NSString*)currency forAmount:(NSDecimalNumber *)amount withReference:(NSString *)reference options:(NSDictionary *)options statusUpdateBlock:(void (^)(PEDPaymentRequest *))statusUpdateBlock {

    PEDPaymentRequest * request = [[PEDPaymentRequest alloc] initWithConnection:self forAmount:amount inCurrency:currency withReference:reference statusUpdateBlock:statusUpdateBlock];

    self.currentRequest = request;
    request.status = kPEDPaymentStatusInProgress;
    
    SQRDCheckoutParameters * params = [[SQRDCheckoutParameters alloc] initWithAmountMoney:[[SQRDMoney alloc] initWithAmount:[amount decimalNumberByMultiplyingBy:[NSDecimalNumber decimalNumberWithString:@"100"]].integerValue currencyCode:SQRDCurrencyCodeMake(currency)]];
    params.note = [NSString stringWithFormat:@"Reference %@. %@ [%@]", reference, self.integrationName, self.client];
    params.skipReceipt = self.skipReceipt;
    
    SQRDCheckoutController * vc = [[SQRDCheckoutController alloc] initWithParameters:params delegate:self];
    
    [vc presentFromViewController:[PEDUtil topMostController]];

    
    return request;
}

//https://developer.squareup.com/docs/payments-api/refund-payments
- (PEDRefundRequest *) refundPaymentReference:(NSString*)reference amount:(NSDecimalNumber *)amount currency:(NSString *)currency withReason:(NSString *)reason statusUpdateBlock:(void(^)(PEDRefundRequest * request))statusUpdateBlock {
    
    PEDRefundRequest * request = [[PEDRefundRequest alloc] initWithConnection:self forTransactionReference:reference forAmount:amount inCurrency:currency withReason:reason statusUpdateBlock:statusUpdateBlock];
    
    if (!reference.length) {
        NSLog(@"Refund failed!: Reference Required.");
        request.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Reference required."]}];
        request.status = kPEDPaymentStatusError;
    } else if (amount && ([amount isEqual:[NSDecimalNumber notANumber]] || (!amount.doubleValue))) {
        NSLog(@"Refund failed!: Invalid amount.");
        request.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Invalid amount."]}];
        request.status = kPEDPaymentStatusError;
    } else {
        PEDSquareConnection * _self = self;
        self.refundDataTask = [self squareDataTask:[NSString stringWithFormat:@"https://connect.squareup.com/v2/payments/%@", reference] method:@"GET" withRequest:nil completionHander:^(NSData * _Nullable data, NSURLResponse * _Nullable getResponse, NSError * _Nullable error) {
            
           _self.refundDataTask = nil;
           
           if (error) {
               request.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Could not fetch payment to determine currency: %@", error.localizedDescription]}];
               request.status = kPEDPaymentStatusError;
           } else {
               NSDictionary * getResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                       
               NSDictionary * body = @{
                   @"idempotency_key": [[NSUUID UUID] UUIDString],
                   @"payment_id": reference,
                   @"amount_money": @{
                           @"amount": [amount decimalNumberByMultiplyingByPowerOf10:2],
                           @"currency": [getResponse valueForKeyPath:@"payment.amount_money.currency"]
                   }
               };
               
               PEDSquareConnection * _self = self;
               _self.refundDataTask = [self squareDataTask:@"https://connect.squareup.com/v2/refunds" method:@"POST" withRequest:body completionHander:^(NSData * _Nullable data, NSURLResponse * _Nullable zresponse, NSError * _Nullable error) {
                   
                   _self.refundDataTask = nil;
                   
                   if (error) {
                       request.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Communication error: %@", error.localizedDescription]}];
                       request.status = kPEDPaymentStatusError;
                   } else {
                       NSDictionary * refundResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                       
                       if (error) {
                           request.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Could not deserialise reponse: %@", error.localizedDescription]}];
                           request.status = kPEDPaymentStatusError;
                       } else if ([refundResponse valueForKey:@"errors"]) {
                           request.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[[refundResponse valueForKeyPath:@"errors.detail"] allObjects].firstObject ? [[refundResponse valueForKeyPath:@"errors.detail"] allObjects].firstObject : @"Could not refund"}];
                           request.status = kPEDPaymentStatusDeclined;
                       } else {
                           if ([[refundResponse valueForKeyPath:@"refund.status"] isEqualToString:@"PENDING"] || [[refundResponse valueForKeyPath:@"refund.status"] isEqualToString:@"COMPLETED"]) {
                               request.gatewayReference = [refundResponse valueForKeyPath:@"refund.id"];
                               request.paymentValues = [refundResponse objectForKey:@"refund"];
                               request.error = nil;
                               request.status = kPEDPaymentStatusSuccess;
                           } else {
                               request.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[refundResponse valueForKeyPath:@"refund.status"] ? [refundResponse valueForKeyPath:@"refund.status"] : @"No return status"}];
                               request.status = kPEDPaymentStatusDeclined;
                           }
                       }
                   }
               }];

               [self.refundDataTask resume];
           }
        }];
        
        [self.refundDataTask resume];
    }
    
    return request;
}

- (NSURLSessionDataTask *) squareDataTask:(NSString *)url method:(NSString *)method withRequest:(NSDictionary *)request completionHander: (void (^)(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)) completionHander {
    
    if (self.debug) {
        NSLog(@"Request [%@ %@]: %@", method, url, request ? [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:request options:0 error:nil] encoding:NSUTF8StringEncoding] : @"n/a");
    }
    NSMutableURLRequest * urlRequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:url]];
    urlRequest.HTTPMethod = method;
    
//    NSString * bearerToken = @"EAAAEPgHEQYqRlbFQeVSQYP5H0IbMTQPATvKeZ0R3d3UQOxtJjIYu_92cIuyB8yw";
    [urlRequest addValue:[NSString stringWithFormat:@"Bearer %@", self.apiKey] forHTTPHeaderField:@"Authorization"];
    if ([method isEqualToString:@"POST"]){
        [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        urlRequest.HTTPBody =  [NSJSONSerialization dataWithJSONObject:request options:0 error:nil];
    }
    
    NSURLSessionDataTask * dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            NSLog(@"squareDataTask Result [%@] ERROR: %@", url, error.localizedDescription);
        } else if (self.debug) {
            NSLog(@"Result [%@]: %@", url, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        }
        
        completionHander(data, response, error);
    }];
    
    return dataTask;
}

- (void) cancelPayment:(PEDPaymentRequest *)payment {
    NSLog(@"cancelPayment not supported");
}

- (void)checkoutController:(SQRDCheckoutController *)checkoutController didFinishCheckoutWithResult:(SQRDCheckoutResult *)result {
    NSLog(@"Transaction successful!");
    
    PEDPaymentRequest * request = self.currentRequest;
    self.currentRequest = nil;

    SQRDTender * tender = nil;
    for (SQRDTender * t in result.tenders.allObjects) {
        if (t.type == SQRDTenderTypeCard) {
            tender = t;
        }
    }
    NSString * cardType = tender.cardDetails.card.brand == SQRDCardBrandVisa ? @"VISA" :
        tender.cardDetails.card.brand == SQRDCardBrandMastercard ? @"MASTERCARD" :
        tender.cardDetails.card.brand == SQRDCardBrandDiscover ? @"DISCOVER" :
        tender.cardDetails.card.brand == SQRDCardBrandAmericanExpress ? @"AMEX" :
        tender.cardDetails.card.brand == SQRDCardBrandDiscoverDiners ? @"DINERS" :
        // tender.cardDetails.card.brand == SQRDCardBrandInterac ? @"INTERAC" :
        tender.cardDetails.card.brand == SQRDCardBrandJCB ? @"JCB" :
        tender.cardDetails.card.brand == SQRDCardBrandSquareGiftCard ? @"GIFTCARD" :
        // tender.cardDetails.card.brand == SQRDCardBrandEftpos ? @"EFTPOS" :
        tender.cardDetails.card.brand == SQRDCardBrandChinaUnionPay ? @"MASTERCARD" : @"OTHER";

    NSString * cardEnding = tender.cardDetails.card.lastFourDigits;
    NSString * cardEntryMode = tender.cardDetails.entryMethod == SQRDTenderCardDetailsEntryMethodChip ? @"CHIP" :
        tender.cardDetails.entryMethod == SQRDTenderCardDetailsEntryMethodSwipe ? @"SWIPE" :
        tender.cardDetails.entryMethod == SQRDTenderCardDetailsEntryMethodContactless ? @"CONTACTLESS" :
        tender.cardDetails.entryMethod == SQRDTenderCardDetailsEntryMethodManuallyEntered ? @"MANUAL" : @"UNKOWN";
    NSString * tenderId = tender.tenderID;
    NSString * authCode = tender.cardDetails.cardReceiptDetails.authorizationCode;

    request.error = nil;
    request.authorisationCode = authCode;
    request.methodType = cardType;
    request.paymentValues = @{
                            @"transactionId": tenderId ? tenderId : [NSNull null],//gateway-reference
                            @"transactionClientId": result.transactionClientID ? result.transactionClientID : [NSNull null],
                              @"transactionIdSDK": result.transactionID ? result.transactionID : [NSNull null],
                              @"authCode": authCode ? authCode : [NSNull null],
                              @"paymentMethodType": cardType ? cardType : [NSNull null],
                              @"paymentMethodDetail": cardEnding ? cardEnding : [NSNull null],
                              @"paymentMethodEntryMode": cardEntryMode ? cardEntryMode : [NSNull null],
                              @"tenderId": tenderId ? tenderId : [NSNull null]
                              };
    request.status = kPEDPaymentStatusSuccess;
}

- (void)checkoutController:(nonnull SQRDCheckoutController *)checkoutController didFailWithError:(nonnull NSError *)error NS_SWIFT_NAME(checkoutController(_:didFailWith:)) {

    PEDPaymentRequest * request = self.currentRequest;

    self.currentRequest = nil;

    if ([error.userInfo[@"com.squareup.SquareReaderSDK.ErrorDebugCode"] isEqualToString:@"checkout_card_processing_not_activated"]) {
        [PEDUtil alertWithTitle:@"Sorry!" message:@"The authorized Square location is not activated for card processing."];
    } else if ([error.userInfo[@"com.squareup.SquareReaderSDK.ErrorDebugCode"] isEqualToString:@"checkout_currency_not_supported_for_location"]) {
        [PEDUtil alertWithTitle:@"Sorry!" message:@"The currency of this transaction does not match the currency on the authorized Square location."];
    } else {
        [PEDUtil alertWithTitle:@"Sorry!" message:[NSString stringWithFormat: @"An unexpected error occurred: %@", error.userInfo[@"com.squareup.SquareReaderSDK.ErrorDebugCode"]]];

    }

    request.paymentValues = @{
        @"error": error.localizedDescription ? error.localizedDescription : [NSNull null]
     };
    request.error = error;
    request.status =  kPEDPaymentStatusError;
}

- (void)checkoutControllerDidCancel:(nonnull SQRDCheckoutController *)checkoutController {
    PEDPaymentRequest * request = self.currentRequest;
    self.currentRequest = nil;

    if (![request.status isEqualToString:kPEDPaymentStatusSuccess] && ![request.status isEqualToString:kPEDPaymentStatusError]) {
        request.status =  kPEDPaymentStatusCancelled;
    }
}


- (void)readerSettingsController:(nonnull SQRDReaderSettingsController *)readerSettingsController didFailToPresentWithError:(nonnull NSError *)error {
    if (![self.status isEqualToString:kPEDStatusConnectingFailed]) {
        [PEDUtil alertWithTitle:@"Sorry!" message:[NSString stringWithFormat:@"Failed to open Square: %@", error.localizedDescription]];
        self.status = kPEDStatusConnectingFailed;
         self.error = error;
        self.statusUpdateBlock(self);
    }
    
    if (self.firmwareCompletionHandler) {
        self.firmwareCompletionHandler(false, [NSString stringWithFormat:@"Failed to open Square: %@", error.localizedDescription]);
        self.firmwareCompletionHandler = nil;
    }
}

- (void)readerSettingsControllerDidPresent:(nonnull SQRDReaderSettingsController *)readerSettingsController {
    NSLog(@"readerSettingsControllerDidPresent");
    if (self.firmwareCompletionHandler) {
        self.firmwareCompletionHandler(true, nil);
        self.firmwareCompletionHandler = nil;
    }
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    if (self.bluetoothManager.state == CBManagerStateUnauthorized) {
        if (![self.status isEqualToString:kPEDStatusConnectingFailed]) {
            [PEDUtil alertWithTitle:@"Sorry!" message:@"To use Square you must go to General Settings and enable bluetooth"];
        }
    }
    else if (self.bluetoothManager.state == CBManagerStatePoweredOff) {
        if (![self.status isEqualToString:kPEDStatusConnectingFailed]) {
            [PEDUtil alertWithTitle:@"Sorry!" message:@"To use Square you must turn on bluetooth"];
        }
    }
}

-(void) updateFirmwareOnComplete:(void (^)(BOOL, NSString *))completionHandler {
    self.firmwareCompletionHandler = completionHandler;
    SQRDReaderSettingsController * vc = [[SQRDReaderSettingsController alloc] initWithDelegate:self];
    [vc presentFromViewController:[PEDUtil topMostController]];
}

@end
