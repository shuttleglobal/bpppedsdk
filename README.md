You need the latest cocoapods (I'm using 1.10.1):
```
sudo gem update cocoapods 
```

Example POD File:
```
source 'https://github.com/CocoaPods/Specs.git'
source 'https://phil-pwb@bitbucket.org/paywithbolt/pods.git'

platform :ios, '12.2'
use_frameworks!

target 'MyApp' do
  pod 'BPPPEDSDK', '0.5.7'
end

post_install do |installer|
  installer.pods_project.targets.each do |target|
    target.build_configurations.each do |config|
      config.build_settings['ENABLE_BITCODE'] = 'NO'
      config.build_settings['CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES'] = 'YES'
      config.build_settings['ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES'] = 'YES'
      config.build_settings['GCC_PREPROCESSOR_DEFINITIONS'] ||= ['$(inherited)', 'INCLUDE_ROAM_AUDIO=1']
    end
  end
end
```

You need to "sign" the square SDK. Under build phases, add a "Run Script" and paste the following script:
```
FRAMEWORKS="${BUILT_PRODUCTS_DIR}/${FRAMEWORKS_FOLDER_PATH}"
"${FRAMEWORKS}/SquareReaderSDK.framework/setup"
```
