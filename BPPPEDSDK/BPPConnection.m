//
//  BPPConnection.m
//  BPPPEDSDK
//
//  Created by Phil Peters on 25/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import "BPPConnection.h"
#import "PEDReachability.h"
#import "PEDRefundRequest+Private.h"

@interface BPPConnection()
@property (nonatomic, retain) NSString * accountCode;
@property (nonatomic, retain) NSString * paymentMethodId;
@property (nonatomic, copy) void (^statusUpdateBlock)(BPPConnection *conn);
@property (nonatomic, retain) PEDReachability * reachability;

@property (nonatomic, retain) PEDConnection * ped;
@property (nonatomic, assign) BOOL dispatchLock;

@property (nonatomic, retain) NSString * host;
@property (nonatomic, retain) NSString * apiKey;
@property (nonatomic, assign) BOOL connecting;
@end

@implementation BPPConnection

+ (id) connectionToHost:(NSString *)host usingApiKey:(NSString *)apiKey accountCode:(NSString *)accountCode paymentMethod:(NSString *)paymentMethodId statusUpdateBlock:(void(^)(BPPConnection *conn))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate {
    BPPConnection * bpp = [[BPPConnection alloc] init];
    bpp.accountCode = accountCode;
    bpp.paymentMethodId = paymentMethodId;
    bpp.host = host;
    bpp.apiKey = apiKey;
    bpp.statusUpdateBlock = statusUpdateBlock;
    bpp.configDelegate = configDelegate;


    if ([accountCode isEqualToString:@"demo"]) {
        bpp.ped = [PEDConnection PEDDemoConnectionWithType:paymentMethodId statusUpdateBlock:^(PEDConnection * conn) {
            statusUpdateBlock(bpp);
        } configDelegate:configDelegate];
    } else {
        [bpp downloadConfig];
    }

    bpp.reachability = [PEDReachability reachabilityForInternetConnection];
    [[NSNotificationCenter defaultCenter] addObserver:bpp selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    [bpp.reachability startNotifier];

    return bpp;
}

- (void) downloadConfig {
    if (!self.ped && !self.connecting) {
        self.connecting = TRUE;
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/ped?accountCode=%@&paymentMethod=%@", self.host, self.accountCode, self.paymentMethodId]];
        // Add Api Key
        
        NSLog(@"BPPConnection.downloadConfig: %@", url.absoluteString);
        BPPConnection * _self = self;
        [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (error) {
                // Dont call callback because we can't handle an error without a PEDConnection
                // On network status change we should try to connect again
            } else {
                NSDictionary * config = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                NSData * pedConnection = config[@"pedConnection"] ? [[NSData alloc] initWithBase64EncodedString:config[@"pedConnection"] options:0] : nil;
                NSDictionary * pedConfig = pedConnection ? [NSJSONSerialization JSONObjectWithData:pedConnection options:0 error:&error] : nil;
                
                dispatch_async( dispatch_get_main_queue(), ^{
                   _self.ped = [PEDConnection PEDConnectionWithConfiguration:pedConfig statusUpdateBlock:^(PEDConnection * conn) {
                        _self.statusUpdateBlock(_self);
                    } configDelegate:_self.configDelegate];
                });
            }
            self.connecting = FALSE;
        }] resume];
    }
}


- (void) reachabilityChanged:(NSObject *) notification {
    if (!self.ped) {
        NSLog(@"Reachability Changes: %d", (int)self.reachability.currentReachabilityStatus);
        [self downloadConfig];
    }
}

- (PEDPaymentRequest *) startPaymentInCurrency:(NSString*)currency forAmount:(NSDecimalNumber *)amount withTransactionReference:(NSString *)reference salesOrderCustomerReference:(NSString *)salesOrderReference statusUpdateBlock:(void(^)(PEDPaymentRequest * request))statusUpdateBlock {
    
    if (!reference) {
        @throw [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:INVALID_SDK userInfo:@{NSLocalizedDescriptionKey: @"Missing transaction reference"}];
    }

    if (!salesOrderReference) {
        @throw [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:INVALID_SDK userInfo:@{NSLocalizedDescriptionKey: @"Missing order reference"}];
    }

    __block BOOL submittedToBrightpearl = false;
    
    BPPConnection * _self = self;
    return [self.ped startPaymentInCurrency:currency forAmount:amount withReference:reference options:nil statusUpdateBlock:^(PEDPaymentRequest *request) {
//        NSLog(@"BPPConnection.statusUpdateBlock: %@", request.status);
        if (!submittedToBrightpearl && ([request.status isEqualToString:kPEDPaymentStatusSuccess] || [request.status isEqualToString:kPEDPaymentStatusDeclined] || [request.status isEqualToString:kPEDPaymentStatusError])) {
            // Save to BPP
            submittedToBrightpearl = true;
            NSLog(@"Saving to brightpearl with order reference: %@", salesOrderReference);
            
            NSDictionary * payload = [_self requestPayload:request forOrder:salesOrderReference];
            [_self saveRequestPayload: payload];
            [_self dispatchPayload:payload onCompletion:^(NSError * error) {
                if (error) {
                    NSLog(@"Could not submit to server: %@", error);
                }
            }];
        }
        statusUpdateBlock(request);
    }];
}

- (NSDictionary *) requestPayload:(PEDPaymentRequest *) request forOrder:(NSString *)salesOrderReference {
    return @{
       @"status": request.status,
       @"accountCode": self.accountCode,
       @"paymentMethodId": self.paymentMethodId,
       @"whenProcessedTime": [NSNumber numberWithInteger:[NSDate timeIntervalSinceReferenceDate]],
       @"customerReference": salesOrderReference,
       @"customerId": request.customerId ? request.customerId : [NSNull null],
       @"transactionReference": request.reference,
       @"currency": request.currency,
       @"amount": request.amount,
       @"methodType": request.methodType ? request.methodType : @"",
       @"methodDescription": request.methodDescription ? request.methodDescription : [NSNull null],
       @"gatewayAuthorisation": request.authorisationCode ? request.authorisationCode : [NSNull null],
       @"gatewayReference": request.gatewayReference ? request.gatewayReference : [NSNull null],
       @"paymentValues": request.paymentValues ? request.paymentValues : @{}
    };
}

- (NSDictionary *) refundRequestPayload:(PEDRefundRequest *) request {
    return @{
        @"accountCode": self.accountCode,
        @"whenProcessedTime": [NSNumber numberWithInteger:[NSDate timeIntervalSinceReferenceDate]],
        @"action": @"REFUND",
        @"refund": @{
           @"status": request.status,
           @"paymentMethodId": self.paymentMethodId,
           @"transactionReference": request.reference,
           @"currency": request.currency,
           @"amount": request.amount,
           @"methodType": request.methodType ? request.methodType : @"",
           @"methodDescription": request.methodDescription ? request.methodDescription : [NSNull null],
           @"gatewayAuthorisation": request.authorisationCode ? request.authorisationCode : [NSNull null],
           @"gatewayReference": request.gatewayReference ? request.gatewayReference : [NSNull null],
           @"paymentValues": request.paymentValues ? request.paymentValues : @{}
        }
    };
}
- (void) saveRequestPayload:(NSDictionary *) payload {
    NSString *filePath = [self.dispatchMessagePath stringByAppendingPathComponent:((NSNumber *)[payload objectForKey:@"whenProcessedTime"]).stringValue];

    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&error];

    if (error) {
        // Save to
        NSLog(@"Could not encode message to file: %@", error.localizedDescription);
    }
    
    [jsonData writeToFile:filePath options:0 error:&error];
    
    if (error) {
        // Save to
        NSLog(@"Could not write dipatch message to file: %@", error.localizedDescription);
    }
}

- (void) deleteRequestPayload:(NSDictionary *) payload {
    NSString * filename = [self.dispatchMessagePath stringByAppendingPathComponent:((NSNumber *)[payload objectForKey:@"whenProcessedTime"]).stringValue];

    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:filename error:&error];

    if (error) {
        // Save to
        NSLog(@"Could not delete file: %@ - %@", filename, error.localizedDescription);
    }
}

- (void) dispatchUnsentMessages {
    if (!self.dispatchLock) {
        self.dispatchLock = true;
        
        NSString* fileName = [[[NSFileManager defaultManager] contentsOfDirectoryAtPath:self.dispatchMessagePath error:NULL] firstObject];
        if (fileName) {
            NSData * data = [NSData dataWithContentsOfFile:[self.dispatchMessagePath stringByAppendingPathComponent:fileName]];
            NSDictionary *payload = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

            [self dispatchPayload:payload onCompletion:^(NSError * error) {
            }];
        }
    }
}

- (void) dispatchPayload:(NSDictionary *) payload onCompletion:(void (^)(NSError *))completionBlock {
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:payload options:0 error:nil];

    // Send and unlock
    NSString * postTransactionUrl = [NSString stringWithFormat:@"%@/api/pedPayment?accountCode=%@", self.host, [payload objectForKey:@"accountCode"]];
    
    //Emulate failure - postTransactionUrl = @"https://httpstat.us/500";
    NSMutableURLRequest * dispatchRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:postTransactionUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60];
    dispatchRequest.HTTPMethod = @"POST";
    [dispatchRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSString *authStr = [NSString stringWithFormat:@"%@", self.apiKey];
    NSData *authData = [authStr dataUsingEncoding:NSASCIIStringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [dispatchRequest setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    BPPConnection * _self = self;
    NSURLSessionUploadTask * task = [[NSURLSession sharedSession] uploadTaskWithRequest:dispatchRequest fromData:jsonData completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        if (error) {
            NSLog(@"Dispatch %@ ERROR: %@", [payload objectForKey:@"transactionReference"], error.localizedDescription);
            _self.dispatchLock = false;

            [_self performSelector:@selector(dispatchUnsentMessages) withObject:nil afterDelay:10];
        
            if (completionBlock)  {
                completionBlock(error);
            };
        } else if (httpResponse.statusCode != 200) {
            if ([payload objectForKey:@"ignore_error"]) {
                [_self deleteRequestPayload:payload];
            }
            NSLog(@"Dispatch to BPP %@ ERROR: Invalid response code (%d): %@", [payload objectForKey:@"transactionReference"], (int)httpResponse.statusCode, [NSString stringWithUTF8String:[data bytes]]);
            _self.dispatchLock = false;
            [_self performSelector:@selector(dispatchUnsentMessages) withObject:nil afterDelay:10];
            
            if (completionBlock)  {
                completionBlock(error);
            };
        } else {
            [_self deleteRequestPayload:payload];
            NSLog(@"Dispatch to BPP %@ complete OK.", [payload objectForKey:@"transactionReference"]);
            
            _self.dispatchLock = false;
            [_self performSelectorInBackground:@selector(dispatchUnsentMessages) withObject:nil];
        }
    }];
    [task resume];
}


- (NSString *) dispatchMessagePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"bpp"];
    // Create if not exists
    [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    
    return path;
}

- (void) cancelPayment:(PEDPaymentRequest *)paymentRequest {
    [self.ped cancelPayment:paymentRequest];
}

- (void) refundTransactionReference:(NSString *)reference amount:(NSDecimalNumber *)amount withReason:(NSString *) reason statusUpdateBlock:(void(^)(PEDRefundRequest * request))statusUpdateBlock {
    // Look up the transaction
    if (self.ped && !self.connecting) {
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/pedPayment?accountCode=%@&paymentMethod=%@&transactionReference=%@", self.host, self.accountCode, self.paymentMethodId, reference]];
        // Add Api Key
        
        NSLog(@"BPPConnection.refundTransactionReference: %@", url.absoluteString);
        BPPConnection * _self = self;
        [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (error) {
                // Dont call callback because we can't handle an error without a PEDConnection
                // On network status change we should try to connect again
                PEDRefundRequest *request = [[PEDRefundRequest alloc] initWithConnection:self.ped forTransactionReference:reference forAmount:amount inCurrency:nil withReason:reason statusUpdateBlock:statusUpdateBlock];
                request.error = error;
                request.status = kPEDPaymentStatusError;
            } else {
                NSDictionary * results = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                NSString *gatewayReference = [results valueForKeyPath:@"transaction.gatewayReference"];
                NSString *currency = [results valueForKeyPath:@"transaction.currency.isoCode"];

                if (gatewayReference) {
                    dispatch_async( dispatch_get_main_queue(), ^{
                        [_self.ped refundPaymentReference:gatewayReference amount:(amount != nil ? amount : [NSDecimalNumber decimalNumberWithDecimal:((NSNumber *)[results valueForKeyPath:@"transaction.totalPaid"]).decimalValue]) currency:currency withReason:reason statusUpdateBlock:^(PEDRefundRequest *request) {
                            // In future - may need to augment with some extra logic, but for now (Paypal Here only) the transaction has been refunded and stored in Bolt.
                            statusUpdateBlock(request);
                            
                            // Send to bolt
                            NSLog(@"Saving to brightpearl with order reference: %@", reference);
                            
                            NSDictionary * payload = [_self refundRequestPayload:request];
                            [_self saveRequestPayload: payload];
                            [_self dispatchPayload:payload onCompletion:^(NSError * error) {
                                if (error) {
                                    NSLog(@"Could not submit to server: %@", error);
                                }
                            }];
                        }];
                    });
                } else {
                    PEDRefundRequest *request = [[PEDRefundRequest alloc] initWithConnection:self.ped forTransactionReference:reference forAmount:amount inCurrency:currency withReason:reason statusUpdateBlock:statusUpdateBlock];
                    request.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:REFERENCE_NOT_FOUND userInfo:@{NSLocalizedDescriptionKey: @"Reference not found, if within a few seconds of transaction you may need to wait a minute."}];
                    request.status = kPEDPaymentStatusError;
                    
                }
            }
            self.connecting = FALSE;
        }] resume];
    }
}


- (void) updateFirmwareOnComplete:(void(^)(BOOL success, NSString *message))completionHandler {
    [self.ped updateFirmwareOnComplete:completionHandler];
}



@end
