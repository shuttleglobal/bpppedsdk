//
//  BPPConnection.h
//  BPPPEDSDK
//
//  Created by Phil Peters on 25/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PEDSDK/PEDSDK.h>

#define INVALID_SDK 1001
#define ERROR 1002
#define CONNECTION_NOT_READY 1003
#define REFERENCE_NOT_FOUND 1004

@interface BPPConnection : NSObject

@property (nonatomic, readonly) NSString * accountCode;
@property (nonatomic, readonly) NSString * paymentMethodId;
@property (nonatomic, readonly) PEDConnection * ped;
@property (nonatomic, readwrite) id<PEDConfigDelegate> configDelegate;

//
// Open a connection when the user logs into the store with the Brightpearl accountCode and paymentMethod.
// You can open multiple connections if you want to support multiple payment methods for a single store.
//
// TESTING:
//  accountCode = <anything>
//  paymentMethodId = 0 Developer Testing
//  paymentMethodId = 2 iZettle (login as externaldemo@izettle.com/externaldemo123)
//
+ (id) connectionToHost:(NSString *)host usingApiKey:(NSString *)apiKey accountCode:(NSString *)accountCode paymentMethod:(NSString *)paymentMethodId statusUpdateBlock:(void(^)(BPPConnection *conn))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate;

//
// Start a payment when the user presses the "CARD" button
// The appropriate SDK will open and take over the screen,
// when cancelled / success / error the callback will be called AFTER the UI has been dismissed
//
- (PEDPaymentRequest *) startPaymentInCurrency:(NSString*)currency forAmount:(NSDecimalNumber *)amount withTransactionReference:(NSString *)reference salesOrderCustomerReference:(NSString *)salesOrderCustomerReference statusUpdateBlock:(void(^)(PEDPaymentRequest * request))statusUpdateBlock;

//
// Cancel a payment
// Please note - this doesn't always work, if we're not in control of the UI, its intended for
// when the SDK DOESNT take over the UI and the user decides to do something else eg cash payment
//
- (void) cancelPayment:(PEDPaymentRequest *)payment;


//
// To issue a refund, create a sales credit and pass it to this function,
// it generally likely WONT touch the UI so you might want to show a spinner
// and block the UI while the refund processes
//
// The call back will always be called on completion.
// 
- (void) refundTransactionReference:(NSString*)reference amount:(NSDecimalNumber *)amount withReason:(NSString *) reason statusUpdateBlock:(void(^)(PEDRefundRequest * request))statusUpdateBlock;


//
// If the status is "kPEDStatusConnectedUpgrade" call this function to start an upgrade
// procedure on the device. The status will be updated to kPEDStatusConnectedUpgrading
//
- (void) updateFirmwareOnComplete:(void(^)(BOOL success, NSString *message))completionHandler;

@end
