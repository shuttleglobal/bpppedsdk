//
//  BPPPEDSDK.h
//  BPPPEDSDK
//
//  Created by Phil Peters on 25/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BPPPEDSDK.
FOUNDATION_EXPORT double BPPPEDSDKVersionNumber;

//! Project version string for BPPPEDSDK.
FOUNDATION_EXPORT const unsigned char BPPPEDSDKVersionString[];

#import "BPPConnection.h"
